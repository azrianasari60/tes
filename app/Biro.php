<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Biro extends Model
{
	protected $table = 'biros';
     public function Proker()
    {
    	return $this->hasMany(Proker::class);
    }
}
