<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alert;
use App\Kementrian;
use App\User;
use App\Biro;
use App\Misi;
use App\Visi;
use App\Tentang;
use App\Program;
use App\Slideshow;
use App\Berita;
use App\Kontak;
use App\pengaduan;
use Session;
use App\Notifications\RepliedToThread;
use Illuminate\Notifications\DatabaseNotification;
class DController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slideshow = slideshow::all()->where('status','Aktiv');
        $kontak = kontak::all()->where('status','Aktiv')->first();
        $berita = berita::orderBy('id','desc')->paginate(3);
        $visi = visi::all()->where('status','Aktiv')->first();
        $tentang = Tentang::where('status','Aktiv')->first();
        $misi = misi::all();
        $program = program::orderBy('id','asc')->get();
        $biro = biro::all();
        // dd($tentang);

        return view('welcome',compact('slideshow','kontak','berita','visi','misi','program','biro','tentang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {

         $pengaduan = new pengaduan;
         $pengaduan->nama  = $request->nama;
         $pengaduan->email  = $request->email;
         $pengaduan->deskripsi  = $request->deskripsi;
         $pengaduan->save() ? alert()->success('Pesan Anda Telah Terkirim', 'Terima Kasih')->autoclose(3000) : '';
         User::all()->each(function ($user) use($pengaduan)
         {
            $user->notify(new RepliedToThread($pengaduan));
         });
         return back();
     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function berita()
    {
        $berita = berita::orderBy('id','desc')->paginate(5);
        return view('berita/halaman-berita',compact('berita'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showberita($id)
    {
        $berita = berita::find($id);
        $beritalist = berita::orderBy('id','desc')->paginate(3);
        return view('berita/berita-click',compact('berita','beritalist'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $q = $request->q;
        $berita = berita::where('judul', 'like', '%' . $q . '%')->paginate(10);
        return view('berita/halaman-berita',compact('berita'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
