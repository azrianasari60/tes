<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Misi;
class MisiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $misi = misi::all();
        return view('admin/misi/index',compact('misi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/misi/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $misi = new Misi();
        $misi->misi = $request->misi;
        $misi->save();
        return redirect('admin/misi');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $misi = misi::find($id);
       return view('admin/misi/edit',compact('misi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $misi = misi::find($id);
        $misi->misi = $request->misi;
        $misi->save();
        return redirect('admin/misi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $misi = misi::find($id);
        $misi->delete();
        return redirect('admin/misi');
    }
    public function deleteOption($id)
    {
        return view('admin/misi/modalmisi',compact('id'));
    }
}
