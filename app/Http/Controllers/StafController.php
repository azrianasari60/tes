<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Staf_bem;
use App\Fakultas;
use Intervention\Image\ImageManagerStatic as Image;
class StafController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staf_bem = staf_bem::orderBy('id','desc')->paginate(20);
        return view('data/staf_bem/index',compact('staf_bem'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $fakultas = fakultas::all();
        return view('data/staf_bem/create',compact('fakultas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $staf_bem= new staf_bem;
        $staf_bem->nama = $request->nama;
        $staf_bem->angkatan = $request->angkatan;
        $staf_bem->fakultas_id = $request->fakultas_id;
        $staf_bem->save();

            return redirect('admin/staf');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
        $staf_bem = staf_bem::find($id);
        $fakultas = fakultas::all();
        return view('data/staf_bem/edit',compact('staf_bem','fakultas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $staf_bem = staf_bem::find($id);
        $staf_bem->nama = $request->nama;
        $staf_bem->angkatan = $request->angkatan;
        $staf_bem->fakultas_id = $request->fakultas_id;
        $staf_bem->save();
         return redirect('admin/staf');


    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $staf_bem = staf_bem::find($id);
        // $staf_bem->$picture->delete();

        // $sosial->$picture->delete();
        $staf_bem->delete();
        return redirect('admin/staf');
    }
public function deleteOption($id)
    {
        return view('data.staf_bem.modalstaf_bem', compact('id'));
    }
}
