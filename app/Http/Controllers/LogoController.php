<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use Session;
use Illuminate\Support\Facades\Input;
class LogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Input::file('file') != null){
         File::delete('images/' . 'img.jpg');
         $fileName = 'img.jpg';
                $path = public_path('images/');
                Input::file('file')->move($path, $fileName);
                // $image = Image::make($file);
                // $image->save($path.$pic);
                return view('admin/logo');
        }else{
            Session::flash('message','Isi foto nya donk');
            return redirect('admin/edit');
        }
    }
    public function filsuf(Request $request)
    {
        if(Input::file('file') != null){
         File::delete('images/' . 'fil_logo.jpg');
         $fileName = 'fil_logo.jpg';
                $path = public_path('images/');
                Input::file('file')->move($path, $fileName);
                // $image = Image::make($file);
                // $image->save($path.$pic);
                return view('admin/logo');
        }else{
            Session::flash('message','Isi foto nya donk');
            return redirect('admin/filsuf');
        }
    }
    public function buatb(Request $request)
    {
        if(Input::file('file') != null){
         File::delete('images/' . 'banner.jpg');
         $fileName = 'banner.jpg';
                $path = public_path('images/');
                Input::file('file')->move($path, $fileName);
                // $image = Image::make($file);
                // $image->save($path.$pic);
                return view('admin/banner');
        }else{
            Session::flash('message','Isi foto nya donk');
            return redirect('banner/edit');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('admin/edit');
    }
    public function buat()
    {
       return view('admin/banner_edit');
    }
public function fil()
    {
       return view('admin/edit_filLogo');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createp()
    {
        return view('admin/edit_struktur');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    if(Input::file('file') != null){
         File::delete('images/' . 'struktur.jpg');
         $fileName = 'struktur.jpg';
                $path = public_path('images/');
       Input::file('file')->move($path, $fileName);
                // $image = Image::make($file);
                // $image->save($path.$pic);
                return view('admin/struktur');
    }else{
            Session::flash('message','Isi foto nya donk');
            return redirect('struktur/edit');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
