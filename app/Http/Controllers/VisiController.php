<?php

namespace App\Http\Controllers;
use App\Visi;
use DB;
use Illuminate\Http\Request;

class VisiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $visi = visi::orderBy('id','desc')->get();

        return view('admin/visi/index',compact('visi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('admin/visi/create');
    }
     public function aktiv($id)
    {
         DB::table('visis')->where('status','Aktiv')->update(['status' => 'Disabled']);
         $visi = visi::find($id);
         $visi->status = 'Aktiv';
         $visi->save();
         return redirect('admin/visi');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $visi = new Visi();
        $visi->visi = $request->visi;
        $visi->save();
        return redirect('admin/visi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $visi = visi::find($id);
        return view('admin/visi/edit',compact('visi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $visi = visi::find($id);
        $visi->visi = $request->visi;
        $visi->save();
        return redirect('admin/visi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
