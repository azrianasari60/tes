<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Kontak;
class KontakController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kontak = kontak::all();
        return view('admin/kontak/index',compact('kontak'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/kontak/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kontak = new Kontak();
        $kontak->alamat = $request->alamat;
        $kontak->line = $request->line;
        $kontak->no_hp = $request->no_hp;
        $kontak->no_telp = $request->no_telp;
        $kontak->save();
        return redirect('admin/kontak');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function aktiv($id)
    {
         DB::table('kontaks')->where('status','Aktiv')->update(['status' => 'Disabled']);
         $slideshow = kontak::find($id);
         $slideshow->status = 'Aktiv';
         $slideshow->save();
         return redirect('admin/kontak');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $kontak = kontak::find($id);
       return view('admin/kontak/edit',compact('kontak'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kontak = kontak::find($id);
        $kontak->alamat = $request->alamat;
        $kontak->line = $request->line;
        $kontak->no_hp = $request->no_hp;
        $kontak->no_telp = $request->no_telp;
        $kontak->save();
        return redirect('admin/kontak');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kontak = kontak::find($id);
        $kontak->delete();
        return redirect('admin/kontak');
    }
    public function deleteOption($id)
    {
        return view('admin/kontak/modalkontak',compact('id'));
    }
}
