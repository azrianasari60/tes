<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ukm;
use App\Staf_bem;
use App\Fakultas;

class DatabaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ukm()
    {
        $ukm = ukm::all();

        return view('bankdata/isi/halaman-ukm',compact('ukm'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fakultas()
    {
      $fakultas = fakultas::all();

      return view('bankdata/isi/halaman-fakultas',compact('fakultas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function staf()
    {
      $staf_bem = staf_bem::all();

      return view('bankdata/isi/halaman-staff',compact('staf_bem')); 
    }
    public function searching_ukm()
    {

    }
    public function searching_staf()
    {
        //
    }
    public function searching_fakultas()
    {
        //
    }

}
