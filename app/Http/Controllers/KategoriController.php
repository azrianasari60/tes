<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\kategori;
use Intervention\Image\ImageManagerStatic as Image;
class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori = kategori::orderBy('id','desc')->paginate(20);
        return view('data/kategori/index',compact('kategori'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('data/kategori/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $kategori= new kategori;
        $kategori->nama = $request->nama;
        $kategori->deskripsi = $request->deskripsi;
        $kategori->model = $request->model;
        $files = $request->file('file');

                $pic = md5($files->getClientOriginalName().rand('123','456')).$files->getClientOriginalName();
                $path = public_path('images/kategori/');
                $image = Image::make($files);
                $image->save($path.$pic);
                $kategori->foto = $pic;
                $kategori->save();


            return redirect('admin/kategori');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
        $kategori = kategori::find($id);
        return view('data/kategori/edit',compact('kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $kategori = kategori::find($id);
         if($request->file != null ){
            $this->delImage($kategori->foto);
        $kategori->nama = $request->nama;
        $kategori->deskripsi = $request->deskripsi;
        $kategori->model = $request->model;
        $file = $request->file('file');

                $pic = md5($file->getClientOriginalName().rand('123','456')).$file->getClientOriginalName();
                $path = public_path('images/kategori/');
                $image = Image::make($file);
                $image->save($path.$pic);
                $kategori->foto = $pic;
                $kategori->save();


            return redirect('admin/kategori');
        }else{
        $kategori->nama = $request->nama;
        $kategori->deskripsi = $request->deskripsi;
        $kategori->model = $request->model;
        $kategori->save();
         return redirect('admin/kategori');
        }

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategori = kategori::find($id);
        // $kategori->$picture->delete();

        // $sosial->$picture->delete();


            $this->delImage($kategori->foto);




        $kategori->delete();
        return redirect('admin/kategori');
    }
public function deleteOption($id)
    {
        return view('data.kategori.modalkategori', compact('id'));
    }
     public function delImage($filename)
    {
        $path = public_path('images/kategori/');
        return \File::delete($path.$filename);
    }
}