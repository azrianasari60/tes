<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ukm;
use Intervention\Image\ImageManagerStatic as Image;
class UkmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ukm = ukm::orderBy('id','desc')->paginate(20);
        return view('data/ukm/index',compact('ukm'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('data/ukm/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $ukm= new ukm;
        $ukm->nama = $request->nama;
        $ukm->nama_ukm = $request->nama_ukm;
        $ukm->deskripsi = $request->deskripsi;
        $ukm->no_hp = $request->no_hp;
$ukm->save();
            return redirect('admin/ukm');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
        $ukm = ukm::find($id);
        return view('data/ukm/edit',compact('ukm'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $ukm = ukm::find($id);
        $ukm->nama = $request->nama;
        $ukm->nama_ukm = $request->nama_ukm;
        $ukm->deskripsi = $request->deskripsi;
        $ukm->no_hp = $request->no_hp;
        $ukm->save();
         return redirect('admin/ukm');
        }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ukm = ukm::find($id);
        // $ukm->$picture->delete();

        // $sosial->$picture->delete();
        $ukm->delete();
        return redirect('admin/ukm');
    }
public function deleteOption($id)
    {
        return view('data.ukm.modalukm', compact('id'));
    }
}
