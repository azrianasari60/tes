<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\pengaduan;
use App\User;
use Session;
use App\Notifications\RepliedToThread;
use Illuminate\Notifications\DatabaseNotification;
class PengaduanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pengaduan = pengaduan::latest()->paginate(10);

        return view('admin/pengaduan/index',compact('pengaduan'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,$notification)
    {
        DatabaseNotification::find($notification)->update(['read_at' => \Carbon\Carbon::now()]);
        $pengaduan = pengaduan::find($id);
        return view('admin/pengaduan/lihat',compact('pengaduan'));
    }
    public function shows($id)
    {

        $pengaduan = pengaduan::find($id);
        return view('admin/pengaduan/lihat',compact('pengaduan'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function destroy($id)
    {
        $pengaduan = pengaduan::find($id);
        $pengaduan->delete();
        return redirect('admin/pengaduan');
    }
public function deleteOption($id)
    {
        return view('admin.pengaduan.modalpengaduan', compact('id'));
    }

}
