<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\kebijakan;
class KebijakanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kebijakan = kebijakan::all();
        return view('data/kebijakan/index',compact('kebijakan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('data/kebijakan/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kebijakan = new kebijakan();
        $kebijakan->nama = $request->nama;
        $kebijakan->save();
        return redirect('admin/kebijakan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $kebijakan = kebijakan::find($id);
       return view('data/kebijakan/edit',compact('kebijakan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kebijakan = kebijakan::find($id);
        $kebijakan->nama = $request->nama;
        $kebijakan->save();
        return redirect('admin/kebijakan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kebijakan = kebijakan::find($id);
        $kebijakan->delete();
        return redirect('admin/kebijakan');
    }
    public function deleteOption($id)
    {
        return view('data/kebijakan/modalkebijakan',compact('id'));
    }
}
