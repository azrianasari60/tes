<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\datasurvey;
use App\kategori;
use Intervention\Image\ImageManagerStatic as Image;
class DatasurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori = kategori::all();
        $datasurvey = datasurvey::orderBy('id','desc')->paginate(20);
        return view('data/datasurvey/index',compact('datasurvey','kategori'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = kategori::all();
        return view('data/datasurvey/create',compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $datasurvey= new datasurvey;
        $datasurvey->judul = $request->judul;
        $datasurvey->deskripsi = $request->deskripsi;
        $datasurvey->kategori_id = $request->kategori_id;
        $files = $request->file('file');

                $pic = md5($files->getClientOriginalName().rand('123','456')).$files->getClientOriginalName();
                $path = public_path('images/datasurvey/');
                $image = Image::make($files);
                $image->save($path.$pic);
                $datasurvey->foto = $pic;
                $datasurvey->save();


            return redirect('admin/datasurvey');


    }

    public function searching(Request $request)
    {
        $q = $request->q;
        $k = $request->kategori;
        $kategori = kategori::all();
        if($k == 'semua'){
        $datasurvey = datasurvey::where('judul', 'like', '%' . $q . '%')->paginate(10);
        return view('data/datasurvey/index',compact('datasurvey','kategori'));
    }else{
         $datasurvey = datasurvey::where('judul', 'like', '%' . $q . '%')->where('kategori_id',$k)->paginate(10);
        return view('data/datasurvey/index',compact('datasurvey','kategori'));
    }

    }

   public function edit($id)
    {
        $datasurvey = datasurvey::find($id);
        $kategori = kategori::all();
        return view('data/datasurvey/edit',compact('datasurvey','kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $datasurvey = datasurvey::find($id);
         if($request->file != null ){
            $this->delImage($datasurvey->foto);
        $datasurvey->judul = $request->judul;
        $datasurvey->deskripsi = $request->deskripsi;
        $datasurvey->kategori_id = $request->kategori_id;
        $file = $request->file('file');

                $pic = md5($file->getClientOriginalName().rand('123','456')).$file->getClientOriginalName();
                $path = public_path('images/datasurvey/');
                $image = Image::make($file);
                $image->save($path.$pic);
                $datasurvey->foto = $pic;
                $datasurvey->save();


            return redirect('admin/datasurvey');
        }else{
        $datasurvey->nama = $request->nama;
        $datasurvey->deskripsi = $request->deskripsi;
        $datasurvey->kategori_id = $request->kategori_id;
        $datasurvey->save();
         return redirect('admin/datasurvey');
        }

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $datasurvey = datasurvey::find($id);
        // $datasurvey->$picture->delete();

        // $sosial->$picture->delete();


            $this->delImage($datasurvey->foto);




        $datasurvey->delete();
        return redirect('admin/datasurvey');
    }
public function deleteOption($id)
    {
        return view('data.datasurvey.modaldatasurvey', compact('id'));
    }
     public function delImage($filename)
    {
        $path = public_path('images/datasurvey/');
        return \File::delete($path.$filename);
    }
}