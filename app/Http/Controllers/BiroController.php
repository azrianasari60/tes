<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Biro;
use App\Proker;
use Intervention\Image\ImageManagerStatic as Image;
class BiroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $biro = Biro::orderBy('id','desc')->get();
        return view('admin/biro/index',compact('biro'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/biro/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $biro = new Biro;
        $biro->nama = $request->nama;
        $file = $request->file('file');
                $pic = md5($file->getClientOriginalName().rand('123','456')).$file->getClientOriginalName();
                $path = public_path('images/biro/');
                $image = Image::make($file);
                $image->save($path.$pic);
                $biro->foto = $pic;
        $files = $request->file('logo');
                $pics = md5($files->getClientOriginalName().rand('123','456')).$files->getClientOriginalName();
                $path = public_path('images/biro/');
                $images = Image::make($files);
                $images->save($path.$pics);
                $biro->logo = $pics;
                if ($biro->save()){
                     $proker = [];
                $x=0;
                 foreach ($request->judul as $judul) {
                $proker[$x]['judul'] = $judul;
                $x++;
                 }
                 $x=0;
                 foreach ($request->deskripsi as $deskripsi) {
                $proker[$x]['deskripsi'] = $deskripsi;
                $x++;
                 }
                 foreach ($proker as $pk) {
                     # code...
                    $proker= new Proker;
                    $proker->biro_id = $biro->id;
                    $proker->judul = $pk['judul'];
                    $proker->deskripsi = $pk['deskripsi'];
                    $proker->save();

                 }
                }

                return redirect('admin/biro');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $biro = biro::find($id);
        return view('admin/biro/edit',compact('biro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if (count($request->file('file')) == 0 && count($request->file('logo')) == 0){
      $biro = biro::find($id);
      $coba = Proker::where('biro_id', $biro->id);
      $coba->delete();
      $biro->nama = $request->nama;
              if ($biro->save()){
                   $proker = [];
              $x=0;
               foreach ($request->judul as $judul) {
              $proker[$x]['judul'] = $judul;
              $x++;
               }
               $x=0;
               foreach ($request->deskripsi as $deskripsi) {
              $proker[$x]['deskripsi'] = $deskripsi;
              $x++;
               }
               foreach ($proker as $pk) {
                   # code...
                  $proker= new Proker;
                  $proker->biro_id = $biro->id;
                  $proker->judul = $pk['judul'];
                  $proker->deskripsi = $pk['deskripsi'];
                  $proker->save();

               }
              }
            }elseif (count($request->file('file')) == 1 && count($request->file('logo')) == 1) {
              $biro = biro::find($id);
              $coba = Proker::where('biro_id', $biro->id);
              $coba->delete();

                  $this->delImage($biro->foto);
                  $this->delImage($biro->logo);


              $biro->nama = $request->nama;
              $file = $request->file('file');
              $pic = md5($file->getClientOriginalName().rand('123','456')).$file->getClientOriginalName();
              $path = public_path('images/biro/');
              $image = Image::make($file);
              $image->save($path.$pic);
              $biro->foto = $pic;
              $files = $request->file('logo');
                      $pics = md5($files->getClientOriginalName().rand('123','456')).$files->getClientOriginalName();
                      $path = public_path('images/biro/');
                      $images = Image::make($files);
                      $images->save($path.$pics);
                      $biro->logo = $pics;
                      if ($biro->save()){
                           $proker = [];
                      $x=0;
                       foreach ($request->judul as $judul) {
                      $proker[$x]['judul'] = $judul;
                      $x++;
                       }
                       $x=0;
                       foreach ($request->deskripsi as $deskripsi) {
                      $proker[$x]['deskripsi'] = $deskripsi;
                      $x++;
                       }
                       foreach ($proker as $pk) {
                           # code...
                          $proker= new Proker;
                          $proker->biro_id = $biro->id;
                          $proker->judul = $pk['judul'];
                          $proker->deskripsi = $pk['deskripsi'];
                          $proker->save();

                       }
                      }
            }
            else{
                if (count($request->file('file')) == 1){
              $biro = biro::find($id);
              $coba = Proker::where('biro_id', $biro->id);
              $coba->delete();

                  $this->delImage($biro->foto);


              $biro->nama = $request->nama;
              $file = $request->file('file');
              $pic = md5($file->getClientOriginalName().rand('123','456')).$file->getClientOriginalName();
              $path = public_path('images/biro/');
              $image = Image::make($file);
              $image->save($path.$pic);
              $biro->foto = $pic;
                      if ($biro->save()){
                           $proker = [];
                      $x=0;
                       foreach ($request->judul as $judul) {
                      $proker[$x]['judul'] = $judul;
                      $x++;
                       }
                       $x=0;
                       foreach ($request->deskripsi as $deskripsi) {
                      $proker[$x]['deskripsi'] = $deskripsi;
                      $x++;
                       }
                       foreach ($proker as $pk) {
                           # code...
                          $proker= new Proker;
                          $proker->biro_id = $biro->id;
                          $proker->judul = $pk['judul'];
                          $proker->deskripsi = $pk['deskripsi'];
                          $proker->save();

                       }
                      }
                    }elseif(count($request->file('logo')) == 1){
                      $biro = biro::find($id);
                      $coba = Proker::where('biro_id', $biro->id);
                      $coba->delete();

                          $this->delImage($biro->logo);


                      $biro->nama = $request->nama;
                      $files = $request->file('logo');
                              $pics = md5($files->getClientOriginalName().rand('123','456')).$files->getClientOriginalName();
                              $path = public_path('images/biro/');
                              $images = Image::make($files);
                              $images->save($path.$pics);
                              $biro->logo = $pics;
                              if ($biro->save()){
                                   $proker = [];
                              $x=0;
                               foreach ($request->judul as $judul) {
                              $proker[$x]['judul'] = $judul;
                              $x++;
                               }
                               $x=0;
                               foreach ($request->deskripsi as $deskripsi) {
                              $proker[$x]['deskripsi'] = $deskripsi;
                              $x++;
                               }
                               foreach ($proker as $pk) {
                                   # code...
                                  $proker= new Proker;
                                  $proker->biro_id = $biro->id;
                                  $proker->judul = $pk['judul'];
                                  $proker->deskripsi = $pk['deskripsi'];
                                  $proker->save();

                               }
                              }
                    }
            }

              return redirect('admin/biro');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $biro = biro::find($id);

        $proker = proker::where('biro_id',$biro->id);
        $proker->delete();
        if ($biro->delete()){
            $this->delImage($biro->foto);
        }
        return redirect('admin/biro');
    }
     public function delImage($filename)
    {
        $path = public_path('images/biro/');
        return \File::delete($path.$filename);
    }
    public function deleteOption($id)
    {
        return view('admin/biro/modalbiro', compact('id'));
    }

}
