<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\going;
use Intervention\Image\ImageManagerStatic as Image;
class GoingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $going = going::orderBy('id','desc')->paginate(20);
        return view('data/going/index',compact('going'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('data/going/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $going= new going;
        $going->judul = $request->judul;
        $going->deskripsi = $request->deskripsi;
        $files = $request->file('file');

                $pic = md5($files->getClientOriginalName().rand('123','456')).$files->getClientOriginalName();
                $path = public_path('images/going/');
                $image = Image::make($files);
                $image->save($path.$pic);
                $going->foto = $pic;
                $going->save();


            return redirect('admin/going');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
        $going = going::find($id);
        return view('data/going/edit',compact('going'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $going = going::find($id);
         if($request->file != null ){
            $this->delImage($going->foto);
        $going->judul = $request->judul;
        $going->deskripsi = $request->deskripsi;
        $file = $request->file('file');

                $pic = md5($file->getClientOriginalName().rand('123','456')).$file->getClientOriginalName();
                $path = public_path('images/going/');
                $image = Image::make($file);
                $image->save($path.$pic);
                $going->foto = $pic;
                $going->save();


            return redirect('admin/going');
        }else{
        $going->judul = $request->judul;
        $going->deskripsi = $request->deskripsi;
        $going->save();
         return redirect('admin/going');
        }

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $going = going::find($id);
        // $going->$picture->delete();

        // $sosial->$picture->delete();


            $this->delImage($going->foto);




        $going->delete();
        return redirect('admin/going');
    }
public function deleteOption($id)
    {
        return view('data.going.modalgoing', compact('id'));
    }
     public function delImage($filename)
    {
        $path = public_path('images/going/');
        return \File::delete($path.$filename);
    }
}