<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\kontakdata;
class KontakdataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kontakdata = kontakdata::all();
        return view('data/kontakdata/index',compact('kontakdata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('data/kontakdata/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kontakdata = new kontakdata();
        $kontakdata->ig = $request->ig;
        $kontakdata->no_hp = $request->no_hp;
        $kontakdata->email = $request->email;
        $kontakdata->save();
        return redirect('admin/kontakdata');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function aktiv($id)
    {
         DB::table('kontakdata')->where('status','Aktiv')->update(['status' => 'Disabled']);
         $slideshow = kontakdata::find($id);
         $slideshow->status = 'Aktiv';
         $slideshow->save();
         return redirect('admin/kontakdata');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $kontakdata = kontakdata::find($id);
       return view('data/kontakdata/edit',compact('kontakdata'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kontakdata = kontakdata::find($id);
        $kontakdata->ig = $request->ig;
        $kontakdata->email = $request->email;
        $kontakdata->no_hp = $request->no_hp;
        $kontakdata->save();
        return redirect('admin/kontakdata');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kontakdata = kontakdata::find($id);
        $kontakdata->delete();
        return redirect('admin/kontakdata');
    }
    public function deleteOption($id)
    {
        return view('data/kontakdata/modalkontakdata',compact('id'));
    }
}
