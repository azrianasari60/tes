<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kementrian;
use App\Proker;
use Intervention\Image\ImageManagerStatic as Image;
class KementrianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kementrian = kementrian::orderBy('id','desc')->get();
        return view('admin/kementrian/index',compact('kementrian'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/kementrian/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kementrian = new kementrian;
        $kementrian->nama = $request->nama;
        $file = $request->file('file');
                $pic = md5($file->getClientOriginalName().rand('123','456')).$file->getClientOriginalName();
                $path = public_path('images/kementrian/');
                $image = Image::make($file);
                $image->save($path.$pic);
                $kementrian->foto = $pic;
                $files = $request->file('logo');
                        $pics = md5($files->getClientOriginalName().rand('123','456')).$files->getClientOriginalName();
                        $path = public_path('images/kementrian/');
                        $images = Image::make($files);
                        $images->save($path.$pics);
                        $kementrian->logo = $pics;
                if ($kementrian->save()){
                     $proker = [];
                $x=0;
                 foreach ($request->judul as $judul) {
                $proker[$x]['judul'] = $judul;
                $x++;
                 }
                 $x=0;
                 foreach ($request->deskripsi as $deskripsi) {
                $proker[$x]['deskripsi'] = $deskripsi;
                $x++;
                 }
                 foreach ($proker as $pk) {
                     # code...
                    $proker= new Proker;
                    $proker->kementrian_id = $kementrian->id;
                    $proker->judul = $pk['judul'];
                    $proker->deskripsi = $pk['deskripsi'];
                    $proker->save();

                 }
                }

                return redirect('admin/kementrian');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $kementrian = kementrian::find($id);
      return view('admin/kementrian/edit',compact('kementrian'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if (count($request->file('file')) == 0 && count($request->file('logo')) == 0){
      $kementrian = kementrian::find($id);
      $coba = Proker::where('kementrian_id', $kementrian->id);
      $coba->delete();
      $kementrian->nama = $request->nama;
              if ($kementrian->save()){
                   $proker = [];
              $x=0;
               foreach ($request->judul as $judul) {
              $proker[$x]['judul'] = $judul;
              $x++;
               }
               $x=0;
               foreach ($request->deskripsi as $deskripsi) {
              $proker[$x]['deskripsi'] = $deskripsi;
              $x++;
               }
               foreach ($proker as $pk) {
                   # code...
                  $proker= new Proker;
                  $proker->kementrian_id = $kementrian->id;
                  $proker->judul = $pk['judul'];
                  $proker->deskripsi = $pk['deskripsi'];
                  $proker->save();

               }
              }
            }elseif (count($request->file('file')) == 1 && count($request->file('logo')) == 1) {
              $kementrian = kementrian::find($id);
              $coba = Proker::where('kementrian_id', $kementrian->id);
              $coba->delete();

                  $this->delImage($kementrian->foto);
                  $this->delImage($kementrian->logo);


              $kementrian->nama = $request->nama;
              $file = $request->file('file');
              $pic = md5($file->getClientOriginalName().rand('123','456')).$file->getClientOriginalName();
              $path = public_path('images/kementrian/');
              $image = Image::make($file);
              $image->save($path.$pic);
              $kementrian->foto = $pic;
              $files = $request->file('logo');
                      $pics = md5($files->getClientOriginalName().rand('123','456')).$files->getClientOriginalName();
                      $path = public_path('images/kementrian/');
                      $images = Image::make($files);
                      $images->save($path.$pics);
                      $kementrian->logo = $pics;
                      if ($kementrian->save()){
                           $proker = [];
                      $x=0;
                       foreach ($request->judul as $judul) {
                      $proker[$x]['judul'] = $judul;
                      $x++;
                       }
                       $x=0;
                       foreach ($request->deskripsi as $deskripsi) {
                      $proker[$x]['deskripsi'] = $deskripsi;
                      $x++;
                       }
                       foreach ($proker as $pk) {
                           # code...
                          $proker= new Proker;
                          $proker->kementrian_id = $kementrian->id;
                          $proker->judul = $pk['judul'];
                          $proker->deskripsi = $pk['deskripsi'];
                          $proker->save();

                       }
                      }
            }
            else{
                if (count($request->file('file')) == 1){
              $kementrian = kementrian::find($id);
              $coba = Proker::where('kementrian_id', $kementrian->id);
              $coba->delete();

                  $this->delImage($kementrian->foto);


              $kementrian->nama = $request->nama;
              $file = $request->file('file');
              $pic = md5($file->getClientOriginalName().rand('123','456')).$file->getClientOriginalName();
              $path = public_path('images/kementrian/');
              $image = Image::make($file);
              $image->save($path.$pic);
              $kementrian->foto = $pic;
                      if ($kementrian->save()){
                           $proker = [];
                      $x=0;
                       foreach ($request->judul as $judul) {
                      $proker[$x]['judul'] = $judul;
                      $x++;
                       }
                       $x=0;
                       foreach ($request->deskripsi as $deskripsi) {
                      $proker[$x]['deskripsi'] = $deskripsi;
                      $x++;
                       }
                       foreach ($proker as $pk) {
                           # code...
                          $proker= new Proker;
                          $proker->kementrian_id = $kementrian->id;
                          $proker->judul = $pk['judul'];
                          $proker->deskripsi = $pk['deskripsi'];
                          $proker->save();

                       }
                      }
                    }elseif(count($request->file('logo')) == 1){
                      $kementrian = kementrian::find($id);
                      $coba = Proker::where('kementrian_id', $kementrian->id);
                      $coba->delete();

                          $this->delImage($kementrian->logo);


                      $kementrian->nama = $request->nama;
                      $files = $request->file('logo');
                              $pics = md5($files->getClientOriginalName().rand('123','456')).$files->getClientOriginalName();
                              $path = public_path('images/kementrian/');
                              $images = Image::make($files);
                              $images->save($path.$pics);
                              $kementrian->logo = $pics;
                              if ($kementrian->save()){
                                   $proker = [];
                              $x=0;
                               foreach ($request->judul as $judul) {
                              $proker[$x]['judul'] = $judul;
                              $x++;
                               }
                               $x=0;
                               foreach ($request->deskripsi as $deskripsi) {
                              $proker[$x]['deskripsi'] = $deskripsi;
                              $x++;
                               }
                               foreach ($proker as $pk) {
                                   # code...
                                  $proker= new Proker;
                                  $proker->kementrian_id = $kementrian->id;
                                  $proker->judul = $pk['judul'];
                                  $proker->deskripsi = $pk['deskripsi'];
                                  $proker->save();

                               }
                              }
                    }
            }
              return redirect('admin/kementrian');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
    {
        $kementrian = kementrian::find($id);

        $proker = proker::where('kementrian_id',$kementrian->id);
        $proker->delete();
        if ($kementrian->delete()){
            $this->delImage($kementrian->foto);
        }
        return redirect('admin/kementrian');
    }
     public function delImage($filename)
    {
        $path = public_path('images/kementrian/');
        return \File::delete($path.$filename);
    }
    public function deleteOption($id)
    {
        return view('admin/kementrian/modalkementrian', compact('id'));
    }
}
