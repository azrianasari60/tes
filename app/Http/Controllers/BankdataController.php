<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alert;
use App\Kontak;
use App\datasurvey;
use App\kategori;
use App\kebijakan;
use App\going;
use App\Survey;
use App\Fakultas;
use App\slidebank;
use App\Ukm;
use App\kontakdata;
use App\Staf_bem;
use Session;
use App\Notifications\RepliedToThread;
use Illuminate\Notifications\DatabaseNotification;
class BankdataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori = kategori::where('model','Mahasiswa')->get();
        $kategori1 = kategori::where('model','Pergerakan')->get();
        $survey = survey::orderBy('id','desc')->paginate(5);
        $going = going::orderBy('id','desc')->paginate(5);
        $slidebank = slidebank::all()->where('status','Aktiv');
        $kebijakan = kebijakan::all();
        $kontak = kontakdata::all()->where('status','Aktiv')->first();
        $datas = datasurvey::orderBy('id','asc')->paginate(5);

        return view('bankdata/home',compact('kategori','datas','kategori1','survey','going','slidebank','kebijakan','kontak','data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function tampilsurvey($id)
    {
        $survey = datasurvey::where('kategori_id',$id)->paginate(10);
        $kategori = kategori::all();
        return view('bankdata/isi/halaman-bankdata',compact('survey','kategori'));
    }
    public function kumpulansurvey()
    {
        $survey = datasurvey::orderBy('id','desc')->paginate(10);
        $kategori = kategori::all();
        return view('bankdata/isi/halaman-bankdata',compact('survey','kategori'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function baca($id)
    {
        $datasurvey = datasurvey::find($id);
        $datalist = datasurvey::orderBy('id','desc')->paginate(3);
        $kategori = kategori::all();
        return view('bankdata/isi/data-click',compact('datasurvey','datalist','kategori'));
    }
public function bacaon($id)
    {
        $dataongoing = going::find($id);
        
        return view('bankdata/isi/halaman-ongoing',compact('dataongoing','datalist','kategori'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $q = $request->q;
        $survey = datasurvey::where('judul', 'like', '%' . $q . '%')->paginate(10);
        $kategori = kategori::all();
        return view('bankdata/isi/halaman-bankdata',compact('survey','kategori'));
    }
    public function searching(Request $request)
    {
        $q = $request->q;
        $k = $request->kategori;
        $kategori = kategori::all();
        if($k == 'semua'){
        $survey = datasurvey::where('judul', 'like', '%' . $q . '%')->paginate(10);
        return view('bankdata/isi/halaman-bankdata',compact('survey','kategori'));
    }else{
         $survey = datasurvey::where('judul', 'like', '%' . $q . '%')->where('kategori_id',$k)->paginate(10);
        return view('bankdata/isi/halaman-bankdata',compact('survey','kategori'));
    }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    public function showall()
    {
      $datas = datasurvey::orderBy('id','asc')->paginate(20);
        return view('bankdata/isi/halaman-bankdata',compact('$data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
