<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Survey;
use Intervention\Image\ImageManagerStatic as Image;
class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $survey = survey::orderBy('id','desc')->paginate(20);
        return view('data/survey/index',compact('survey'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('data/survey/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $survey= new survey;
        $survey->judul = $request->judul;
        $survey->deskripsi = $request->deskripsi;
        $files = $request->file('file');

                $pic = md5($files->getClientOriginalName().rand('123','456')).$files->getClientOriginalName();
                $path = public_path('images/survey/');
                $image = Image::make($files);
                $image->save($path.$pic);
                $survey->foto = $pic;
                $survey->save();


            return redirect('admin/survey');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
        $survey = survey::find($id);
        return view('data/survey/edit',compact('survey'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $survey = survey::find($id);
         if($request->file != null ){
            $this->delImage($survey->foto);
        $survey->judul = $request->judul;
        $survey->deskripsi = $request->deskripsi;
        $file = $request->file('file');

                $pic = md5($file->getClientOriginalName().rand('123','456')).$file->getClientOriginalName();
                $path = public_path('images/survey/');
                $image = Image::make($file);
                $image->save($path.$pic);
                $survey->foto = $pic;
                $survey->save();


            return redirect('admin/survey');
        }else{
        $survey->judul = $request->judul;
        $survey->deskripsi = $request->deskripsi;
        $survey->save();
         return redirect('admin/survey');
        }

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $survey = survey::find($id);
        // $survey->$picture->delete();

        // $sosial->$picture->delete();


            $this->delImage($survey->foto);




        $survey->delete();
        return redirect('admin/survey');
    }
public function deleteOption($id)
    {
        return view('data.survey.modalsurvey', compact('id'));
    }
     public function delImage($filename)
    {
        $path = public_path('images/survey/');
        return \File::delete($path.$filename);
    }
}
