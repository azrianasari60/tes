<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;
use App\picture;
use Session;
use Intervention\Image\ImageManagerStatic as Image;
class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $berita = berita::orderBy('id','desc')->paginate(20);
        return view('admin/berita/index',compact('berita'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/berita/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $berita= new Berita;
        $berita->judul = $request->judul;
        $berita->deskripsi = $request->deskripsi;


        if ($berita->save()){
$file = $request->file('file');


                $picture = new picture;
                $picture->berita_id = $berita->id;
                $pic = md5($file->getClientOriginalName().rand('123','456')).$file->getClientOriginalName();
                $path = public_path('images/berita/');
                $image = Image::make($file);
                $image->save($path.$pic);
                $picture->picture = $pic;
                $picture->save();


            return redirect('admin/berita');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $berita = Berita::find($id);
        return view('admin/berita/show',compact('berita'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
        $berita = Berita::find($id);
        return view('admin/berita/edit',compact('berita'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if( count($files = $request->file('file')) ==0  ){
             $berita = Berita::find($id);
             $berita->judul = $request->judul;
                $berita->deskripsi = $request->deskripsi;
                $berita->save();
                return redirect('admin/berita');
        }else{
        $berita = Berita::find($id);
        $picture = picture::where('berita_id',$berita->id);
        if($request->file != null ){
        if ($picture->count() > 0) {
        foreach ($picture->get() as $gal) {
            $this->delImage($gal->picture);
            $gal->delete();
        }
        }
        $berita->judul = $request->judul;
        $berita->deskripsi = $request->deskripsi;

        if ($berita->save()){

            $file = $request->file('file');

                $picture = new picture;
                $picture->berita_id = $berita->id;
                $pic = md5($file->getClientOriginalName().rand('123','456')).$file->getClientOriginalName();
                $path = public_path('images/berita/');
                $image = Image::make($file);
                $image->save($path.$pic);
                $picture->picture = $pic;
                $picture->save();

        }
            return redirect('admin/berita');
        }else{
        $berita->judul = $request->judul;
        $berita->deskripsi = $request->deskripsi;
        $berita->save();
         return redirect('admin/berita');
        }

}

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $berita = Berita::find($id);
        // $berita->$picture->delete();
        $picture = picture::where('berita_id',$berita->id);
        if ($picture->count() > 0) {
        foreach ($picture->get() as $gal) {
            $this->delImage($gal->picture);
            $gal->delete();
        }
        }
        $berita->delete();
        return redirect('admin/berita');
    }
public function deleteOption($id)
    {
        return view('admin.berita.modalberita', compact('id'));
    }
     public function delImage($filename)
    {
        $path = public_path('images/berita/');
        return \File::delete($path.$filename);
    }
}
