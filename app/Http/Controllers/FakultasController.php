<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fakultas;
use Intervention\Image\ImageManagerStatic as Image;
class FakultasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fakultas = fakultas::orderBy('id','desc')->paginate(20);
        return view('data/fakultas/index',compact('fakultas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('data/fakultas/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $fakultas= new fakultas;
        $fakultas->nama = $request->nama;
        $fakultas->fakultas = $request->fakultas;
        $fakultas->no_hp = $request->no_hp;
        $fakultas->save();

            return redirect('admin/fakultas');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
        $fakultas = fakultas::find($id);
        return view('data/fakultas/edit',compact('fakultas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $fakultas = fakultas::find($id);
        $fakultas->nama = $request->nama;
        $fakultas->fakultas = $request->fakultas;
        $fakultas->no_hp = $request->no_hp;
        $fakultas->save();
         return redirect('admin/fakultas');

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fakultas = fakultas::find($id);
        // $fakultas->$picture->delete();

        // $sosial->$picture->delete();
        $fakultas->delete();
        return redirect('admin/fakultas');
    }
public function deleteOption($id)
    {
        return view('data.fakultas.modalfakultas', compact('id'));
    }
}
