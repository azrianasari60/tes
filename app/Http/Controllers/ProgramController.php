<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Program;
use Intervention\Image\ImageManagerStatic as Image;
class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $program = program::orderBy('id','desc')->paginate(20);
        return view('admin/program/index',compact('program'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/program/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $program= new program;
        $program->judul = $request->judul;
        $program->deskripsi = $request->deskripsi;
        $files = $request->file('file');

                $pic = md5($files->getClientOriginalName().rand('123','456')).$files->getClientOriginalName();
                $path = public_path('images/program/');
                $image = Image::make($files);
                $image->save($path.$pic);
                $program->foto = $pic;
                $program->save();


            return redirect('admin/program');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
        $program = program::find($id);
        return view('admin/program/edit',compact('program'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $program = program::find($id);
         if($request->file != null ){
            $this->delImage($program->foto);
        $program->judul = $request->judul;
        $program->deskripsi = $request->deskripsi;
        $file = $request->file('file');

                $pic = md5($file->getClientOriginalName().rand('123','456')).$file->getClientOriginalName();
                $path = public_path('images/program/');
                $image = Image::make($file);
                $image->save($path.$pic);
                $program->foto = $pic;
                $program->save();


            return redirect('admin/program');
        }else{
        $program->judul = $request->judul;
        $program->deskripsi = $request->deskripsi;
        $program->save();
         return redirect('admin/program');
        }

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $program = program::find($id);
        // $program->$picture->delete();

        // $sosial->$picture->delete();


            $this->delImage($program->foto);




        $program->delete();
        return redirect('admin/program');
    }
public function deleteOption($id)
    {
        return view('admin.program.modalprogram', compact('id'));
    }
     public function delImage($filename)
    {
        $path = public_path('images/program/');
        return \File::delete($path.$filename);
    }
}