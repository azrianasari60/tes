<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tentang;
use DB;
use Intervention\Image\ImageManagerStatic as Image;
class TentangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function aktiv($id)
    {
         DB::table('tentangs')->where('status','Aktiv')->update(['status' => 'Disabled']);
         $tentang = tentang::find($id);
         $tentang->status = 'Aktiv';
         $tentang->save();
         return redirect('admin/tentang');
    }
    public function index()
    {
        $tentang = tentang::orderBy('id','desc')->paginate(10);
        return view('admin/tentang/index',compact('tentang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/tentang/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $tentang= new tentang;
        $tentang->nama = $request->judul;
        $tentang->deskripsi = $request->deskripsi;
        $files = $request->file('file');

                $pic = md5($files->getClientOriginalName().rand('123','456')).$files->getClientOriginalName();
                $path = public_path('images/tentang/');
                $image = Image::make($files);
                $image->save($path.$pic);
                $tentang->foto = $pic;
                $tentang->save();


            return redirect('admin/tentang');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
        $tentang = tentang::find($id);
        return view('admin/tentang/edit',compact('tentang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $tentang = tentang::find($id);
         if($request->file != null ){
            $this->delImage($tentang->foto);
        $tentang->nama = $request->judul;
        $tentang->deskripsi = $request->deskripsi;
        $file = $request->file('file');

                $pic = md5($file->getClientOriginalName().rand('123','456')).$file->getClientOriginalName();
                $path = public_path('images/tentang/');
                $image = Image::make($file);
                $image->save($path.$pic);
                $tentang->foto = $pic;
                $tentang->save();


            return redirect('admin/tentang');
        }else{
        $tentang->nama = $request->judul;
        $tentang->deskripsi = $request->deskripsi;
        $tentang->save();
         return redirect('admin/tentang');
        }

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tentang = tentang::find($id);
        // $tentang->$picture->delete();

        // $sosial->$picture->delete();


            $this->delImage($tentang->foto);




        $tentang->delete();
        return redirect('admin/tentang');
    }
public function deleteOption($id)
    {
        return view('admin.tentang.modaltentang', compact('id'));
    }
     public function delImage($filename)
    {
        $path = public_path('images/tentang/');
        return \File::delete($path.$filename);
    }
}