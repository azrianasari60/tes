<?php

namespace App\Http\Controllers;
use App\Slideshow;
use App\picture;
use Illuminate\Http\Request;
use DB;
use Intervention\Image\ImageManagerStatic as Image;
class SlideshowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slideshow = Slideshow::orderBy('id','desc')->paginate(20);

        return view('admin/slideshow/index',compact('slideshow'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/slideshow/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slideshow= new Slideshow;
        $slideshow->nama = $request->nama;
        $slideshow->salam = $request->salam;
        $slideshow->tag = $request->tag;
        $slideshow->tombol = $request->tombol;


        if ($slideshow->save()){

            $files = $request->file('file');
            foreach ($files as $file) {
                $picture = new picture;
                $picture->slideshow_id = $slideshow->id;
                $pic = md5($file->getClientOriginalName().rand('123','456')).$file->getClientOriginalName();
                $path = public_path('images/slideshow/');
                $image = Image::make($file);
                $image->save($path.$pic);
                $picture->picture = $pic;
                $picture->save();
            }

            return redirect('admin/slideshow');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slideshow = Slideshow::find($id);
        return view('admin/slideshow/edit',compact('slideshow'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $slideshow = slideshow::find($id);
        // $slideshow->$picture->delete();
        $picture = picture::where('slideshow_id',$slideshow->id);
         if($request->file != null ){
        if ($picture->count() > 0) {
        foreach ($picture->get() as $gal) {
            $this->delImage($gal->picture);
            $gal->delete();
        }
        }

        $slideshow->nama = $request->nama;
        $slideshow->salam = $request->salam;
        $slideshow->tag = $request->tag;
        $slideshow->tombol = $request->tombol;


        if ($slideshow->save()){

            $files = $request->file('file');
            foreach ($files as $file) {
                $picture = new picture;
                $picture->slideshow_id = $slideshow->id;
                $pic = md5($file->getClientOriginalName().rand('123','456')).$file->getClientOriginalName();
                $path = public_path('images/slideshow/');
                $image = Image::make($file);
                $image->save($path.$pic);
                $picture->picture = $pic;
                $picture->save();
            }
        }
            return redirect('admin/slideshow');
        }else{
        $slideshow->nama = $request->nama;
        $slideshow->salam = $request->salam;
        $slideshow->tag = $request->tag;
        $slideshow->tombol = $request->tombol;
        $slideshow->save();
         return redirect('admin/slideshow');
        }


    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slideshow = slideshow::find($id);
        // $slideshow->$picture->delete();
        $picture = picture::where('slideshow_id',$slideshow->id);
        if ($picture->count() > 0) {
        foreach ($picture->get() as $gal) {
            $this->delImage($gal->picture);
            $gal->delete();
        }
        }
        $slideshow->delete();
        return redirect('admin/slideshow');
    }
public function deleteOption($id)
    {
        return view('admin.slideshow.modalslideshow', compact('id'));
    }
     public function delImage($filename)
    {
        $path = public_path('images/slideshow/');
        return \File::delete($path.$filename);
    }
    public function aktiv($id)
    {
         DB::table('slideshows')->where('status','Aktiv')->update(['status' => 'Disabled']);
         $slideshow = Slideshow::find($id);
         $slideshow->status = 'Aktiv';
         $slideshow->save();
         return redirect('admin/slideshow');
    }
}
