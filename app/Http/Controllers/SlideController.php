<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\slidebank;
use DB;
use Intervention\Image\ImageManagerStatic as Image;
class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slide = slidebank::orderBy('id','desc')->paginate(20);
        return view('data/slide/index',compact('slide'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('data/slide/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $slide= new slidebank;
        $slide->judul = $request->judul;
        $slide->deskripsi = $request->deskripsi;
        $files = $request->file('file');

                $pic = md5($files->getClientOriginalName().rand('123','456')).$files->getClientOriginalName();
                $path = public_path('images/slide/');
                $image = Image::make($files);
                $image->save($path.$pic);
                $slide->foto = $pic;
                $slide->save();


            return redirect('admin/slide');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
        $slide = slidebank::find($id);
        return view('data/slide/edit',compact('slide'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $slide = slidebank::find($id);
         if($request->file != null ){
            $this->delImage($slide->foto);
        $slide->judul = $request->judul;
        $slide->deskripsi = $request->deskripsi;
        $file = $request->file('file');

                $pic = md5($file->getClientOriginalName().rand('123','456')).$file->getClientOriginalName();
                $path = public_path('images/slide/');
                $image = Image::make($file);
                $image->save($path.$pic);
                $slide->foto = $pic;
                $slide->save();


            return redirect('admin/slide');
        }else{
        $slide->judul = $request->judul;
        $slide->deskripsi = $request->deskripsi;
        $slide->save();
         return redirect('admin/slide');
        }

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slide = slidebank::find($id);
        // $slide->$picture->delete();

        // $sosial->$picture->delete();


            $this->delImage($slide->foto);




        $slide->delete();
        return redirect('admin/slide');
    }
public function deleteOption($id)
    {
        return view('data.slide.modalslide', compact('id'));
    }
     public function delImage($filename)
    {
        $path = public_path('images/slide/');
        return \File::delete($path.$filename);
    }
    public function aktiv($id)
    {
         DB::table('slidebanks')->where('status','Aktiv')->update(['status' => 'Disabled']);
         $slidebank = slidebank::find($id);
         $slidebank->status = 'Aktiv';
         $slidebank->save();
         return redirect('admin/slide');
    }
}