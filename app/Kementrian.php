<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kementrian extends Model
{
    protected $table = 'kementrians';
     public function Proker()
    {
    	return $this->hasMany(Proker::class);
    }
}
