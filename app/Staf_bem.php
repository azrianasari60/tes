<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staf_bem extends Model
{
    protected $table ='staf_bems';
    public function Fakultas()
    {
    	return $this->belongsTo(Fakultas::class);
    }
}
