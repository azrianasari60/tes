<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class picture extends Model
{
    protected $table ='pictures';
    protected $fillable = ['slideshow_id',];

    public function slideshow()
    {
        return $this->belongsTo(Slideshow::class);
    }
    public function berita()
    {
        return $this->belongsTo(Berita::class);
    }
}
