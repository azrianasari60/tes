<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proker extends Model
{
    protected $table ='prokers';



    public function Misi()
    {
        return $this->belongsTo(Misi::class);
    }
    public function Visis()
    {
        return $this->belongsTo(Visis::class);
    }
}
