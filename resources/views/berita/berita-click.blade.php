<section id="baca">
@include('berita/header')
<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59fa9249f809021c"></script>



    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div id="baca" class="card">
                    <div class="card-header"><h4 class="text-justify">{{ $berita->judul}}</h4>
                    <small><i class="fa fa-calendar"></i>&nbsp; {{ $berita->created_at }}</small>
                </div>
                    <div class="card-body">
<div class="addthis_inline_share_toolbox_k3xw"></div>
                        @foreach ($berita->picture as $data)

                        <img src="{{ asset('images/berita/'.$data->picture) }}" style="width: 100%; margin-bottom: 20px;">
                        @endforeach
                        <p>{!! $berita->deskripsi !!}</p>
                    </div>
                    <!-- <div class="card-footer">
                        <span class="bagikan">
                            <div class="media">
                              <ul>
                                <li class="bagi">Bagikan di :</li>
                                <li><a href=""><div class="fb"><i class="fa fa-facebook"></i></div></a></li>
                                <li><a href=""><div class="tw"><i class="fa fa-twitter"></i></div></a></li>
                                <li><a href=""><div class="ig"><i class="fa fa-instagram"></i></div></a></li>
                              </ul>
                            </div>
                        </span>
                    </div> -->
                </div>

            </div>
            <div class="col-md-4">
                <div id="berita" class="card">
                    <div class="card-header">
                        <h4>Berita Lainnya</h4>
                    </div>
                    <div class="card-body">
                        @foreach ($beritalist as $data)

                        <div id="konten" class="card" style="width: 100%;">
                            @forelse ($data->picture as $picture)

                          <img class="card-img-top" src="{{ asset('images/berita/'.$picture->picture) }}" @empty kosong @endforelse alt="Card image cap">
                          <div class="card-block">
                            <div class="card-date">{{ $data->created_at }}</div>
                            <div class="card-title">{{ $data->judul }}</div>
                            <div class="card-text text-justify">{!! $data->deskripsi !!}.</div>
                            <div style="text-align: right;"><a href="{{url('baca/'.$data->id)}}" class="">Read more <i class="fa fa-angle-double-right"></i></a></div>
                          </div>
                        </div>
                        <hr>
                        @endforeach

                    </div>
                    <div class="card-footer text-center">
                        <a href="{{url('berita')}}">Baca Lebih Banyak</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

@include('berita/footer')
