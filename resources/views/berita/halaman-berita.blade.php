@include('berita/header')

<section id="halaman-berita">
  <div class="container">

  	<div class="sorting">
  	<div class="row">
  		<div class="col-lg-4 offset-lg-8">
          <form action="{{ url('berita/cari') }}" method="get" enctype="multipart/form-data">
        <div class="input-group search">

            <input type="text" class="form-control" name="q" placeholder="Search for...">
            <span class="input-group-btn">
              <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
            </span>
          </div>
          </form>
  		</div>
  	</div>
  </div>
  @foreach ($berita as $data)
    {{-- expr --}}
    <div class="card">
      <div class="row">
         <div class="col-md-3">
          <div class="card-img-bottom">
            @forelse ($data->picture as $picture)
            <img src="{{ asset('images/berita/'.$picture->picture) }}" @empty kosong @endforelse style="width: 100%; height: 100%;">
          </div>
        </div>
        <div class="col-md-9">
          <div class="card-block">
            <h4 class="card-title">{{ $data->judul}}</h4>
            <div class="card-text">{!! $data->deskripsi !!}</div>
            <a href="{{url('baca/'.$data->id)}}" class="btn btn-primer">Read More <i class="fa fa-angle-double-right"></i></a>
          </div>
        </div>
      </div>
    </div>
    <hr>
  @endforeach




   {!! $berita->render() !!}


  </div>
</section>

@include('berita/footer')
