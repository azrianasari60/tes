<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="robots" content="all,follow">
    <title>BEM KM UNMUL | BERITA</title>
    <link rel="icon" type="image/png" href="/images/img.jpg">
    <!-- <link rel="icon" type="image/png" href="#"> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/bootstrap/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/font-awesome/css/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">

    <!-- <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css"> -->
</head>

<header id="berita-all">

        <div class="berita-banner" style="background-image: url({{asset('images/banner.jpg')}});"></div>

        <nav class="navbar navbar-expand-md navbar-dark bg-faded shrink" style="z-index: 100;">
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <a class="navbar-brand" href="#"><img src="/images/img.jpg" style="height: 50px; top: 50%; transform: translateX(-50%);"></a>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="navigate" href="{{url('/')}}">Beranda</a>
              </li>
              <li class="nav-item">
                <a class="navigate" href="{{url('berita')}}">Berita</a>
              </li>
              <li class="nav-item navigate">
                <a class="disabled" href="{{ url('bankdata') }}">Bank Data</a>
              </li>
            </ul>
          </div>
        </nav>

</header>

<body>
