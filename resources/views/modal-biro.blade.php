<!-- Modal -->
<div class="modal fade modal-biro" id="myModal{{ $data->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{ $data->nama }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-7" style="padding: 0;"><img src="{{ asset('images/biro/'.$data->foto) }}" style="width: 100%;"></div>
          <div class="col-md-5" style="padding: 25px 0;">
          <h5>Program Kerja</h5>
          <table>
            @php
          $n=1;
          @endphp
          @foreach ($data->proker as $program)
            {{-- expr --}}
            <tr>
              <th>{{ $n++.'.' }} </th>
              <th>{{ $program->judul  }} </th>
            </tr>
            <tr>
              <td></td>
              <td>{{ $program->deskripsi  }}</td>
            </tr>
            @endforeach
          </table>

          </div>
        </div>

      </div>
    </div>
    </div>
  </div>
</div>