@extends('layouts.app')
@section('title','UKM')
@section('content')

<div class="container">
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"> Input UKM
				</h3>
				<div class="panel-body">
					<form class="form-horizontal" action="{{url('ukm/store/') }}" method="post" enctype="multipart/form-data" files=true>
						{{csrf_field()}}
						<div class="col-md-6">
							<div class="col-md-6">
							<p>Nama UKM: </p>
							<input type="text" required="" class="form-control"  name="nama_ukm"  >
							</div>
						<div class="col-md-6">
							<p>Nama Pengurus: </p>
							<input type="text" required="" class="form-control"  name="nama"  >
						</div>
						<div class="col-md-6">
							<p>NO HP Pengurus : </p>
							<input type="text" required="" class="form-control"  name="no_hp"  >
						</div>
						<div class="col-md-6">
							<p>Deskripsi : </p>
							<textarea name="deskripsi" class="form-control"></textarea>
						</div>
						<div class="col-md-12">
							<br>
							<button type="submit" class="btn btn-primary">save</button>
						</div>

					</form>
				</div>

		</div>
	</div>
</div>
</div>
@endsection
