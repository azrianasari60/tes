@extends('layouts.app')
@section('title','K Survey')
@section('content')

<div class="container-fluid spark-screen">
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"> Input K Survey
				</h3>
				<div class="panel-body">
					<form class="form-horizontal" action="{{ url('survey/store') }}" method="post" enctype="multipart/form-data" files=true>
						{{csrf_field()}}
						<div class="col-md-6">
							<p>Judul : </p>
							<input type="text" required="" class="form-control" name="judul" value="" >
							</div>
						<div class="col-md-6">
							<p>Picture : </p>
							<input type="file" required="" class="form-control" name="file" value="" >

						</div>
						<div class="col-md-6">
							<p>Deskripsi : </p>
							<textarea type="text" required="" class="form-control" name="deskripsi" ></textarea>
							</div>


						<div class="col-md-12">
							<br>
							<button type="submit" class="btn btn-primary">save</button>
						</div>
					</form>
				</div>

		</div>
	</div>
</div>
</div>
@endsection