@extends('layouts.app')
@section('title','Kategori')
@section('content')

<div class="container">
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"> Input Kategori
				</h3>
				<div class="panel-body">
					<form class="form-horizontal" action="{{url('kategori/store/') }}" method="post" enctype="multipart/form-data" files=true>
						{{csrf_field()}}
						<div class="col-md-6">
							<p>nama : </p>
							<input type="text" required="" class="form-control"  name="nama"  >
							</div>
							<div class="col-md-6">
								<p>Deskripsi : </p>
								<textarea name="deskripsi" required="" class="form-control"></textarea> 
							</div>
						<div class="col-md-6">
							<p>Icon : </p>
							<input type="file" required="" class="form-control" name="file" value="" >
						</div>
						<div class="col-md-6">
							<p>Model : </p>
							<select class="form-control"   name="model">
						      <option>.....</option>
						      <option>Mahasiswa</option>
						      <option>Pergerakan</option>
						  </select>
						</div>
						<div class="col-md-12">
							<br>
							<button type="submit" class="btn btn-primary">save</button>
						</div>

					</form>
				</div>

		</div>
	</div>
</div>
</div>
@endsection
