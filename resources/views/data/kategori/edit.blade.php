@extends('layouts.app')
@section('title','Kategori')
@section('content')

<div class="container">
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"> Edit Kategori
				</h3>
				<div class="panel-body">
					<form class="form-horizontal" action="{{url('kategori/edit/'.$kategori->id) }}" method="post" enctype="multipart/form-data" files=true>
						{{csrf_field()}}
						<div class="col-md-6">
							<p>nama : </p>
							<input type="text" value="{{ $kategori->nama }}" class="form-control"  name="nama"  >
							</div>
						<div class="col-md-6">
							<p>Deskripsi : </p>
							<textarea name="deskripsi" class="form-control">{{ $kategori->deskripsi }}</textarea> 
						</div>
						<div class="col-md-6">
							<p>Icon : </p>
							<input type="file"  class="form-control" name="file" value="" >
						</div>
						<div class="col-md-6">
							<p>Model : </p>
						<select class="form-control"   name="model">

								<option value="{{ $kategori->model }}" @if($kategori->model == $kategori->model) selected="selected"@endif>{{ $kategori->model }} </option>
								@if($kategori->model == 'Mahasiswa')
								<option>Pergerakan</option>
								@else
								<option>Mahasiswa</option>
								@endif
						</select>
						</div>
						<div class="col-md-12">
							<br>
							<button type="submit" class="btn btn-primary">save</button>
						</div>
					</form>
				</div>

		</div>
	</div>
</div>
</div>
@endsection
