@extends('layouts.app')
@section('title','Fakultas')
@section('content')

<div class="container">
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"> Edit Fakultas
				</h3>
				<div class="panel-body">
					<form class="form-horizontal" action="{{url('fakultas/edit/'.$fakultas->id) }}" method="post" enctype="multipart/form-data" files=true>
						{{csrf_field()}}
						<div class="col-md-6">
							<p>Fakultas : </p>
							<input type="text" value="{{ $fakultas->fakultas }}" class="form-control"  name="fakultas"  >
							</div>
						<div class="col-md-6">
							<p>Nama : </p>
							<input type="text" value="{{ $fakultas->nama }}" class="form-control"  name="nama"  >
						</div>
						<div class="col-md-6">
							<p>NO HP : </p>
							<input type="text" value="{{ $fakultas->no_hp }}" class="form-control"  name="no_hp"  >
						</div>
						<div class="col-md-12">
							<br>
							<button type="submit" class="btn btn-primary">save</button>
						</div>
					</form>
				</div>

		</div>
	</div>
</div>
</div>
@endsection