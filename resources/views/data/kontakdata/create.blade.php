@extends('layouts.app')
@section('title','Kontak')
@section('content')

<div class="container">
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"> Input Kontak
				</h3>
				<div class="panel-body">
					<form class="form-horizontal" action="{{url('kontakdata/store/') }}" method="post" enctype="multipart/form-data" files=true>
						{{csrf_field()}}
						<div class="col-md-6">
							<p>Instagram : </p>
							<input type="text" required="" class="form-control"  name="ig"  >
							</div>
						<div class="col-md-6">
							<p>No HP : </p>
							<input type="text" required="" class="form-control"  name="no_hp"  >
							</div>
						<div class="col-md-6">
							<p>Email : </p>
							<input type="email" required="" class="form-control"  name="email"  >
							</div>
						<div class="col-md-12">
							<br>
							<button type="submit" class="btn btn-primary">save</button>
						</div>
					</form>
				</div>

		</div>
	</div>
</div>
</div>
@endsection
