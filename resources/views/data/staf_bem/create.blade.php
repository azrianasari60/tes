@extends('layouts.app')
@section('title','Staff BEMKM')
@section('content')

<div class="container">
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"> Input Staff BEMKM
				</h3>
				<div class="panel-body">
					<form class="form-horizontal" action="{{url('staf_bem/store/') }}" method="post" enctype="multipart/form-data" files=true>
						{{csrf_field()}}
						<div class="col-md-6">
							<div class="col-md-6">
							<p>Nama : </p>
							<input type="text" required="" class="form-control"  name="nama"  >
							</div>
						<div class="col-md-6">
							<p>Fakultas : </p>
							<select required="" class="form-control"   name="fakultas_id">
						      <option>.....</option>
						      @foreach ($fakultas as $data)
						      <option value="{{ $data->id }}">{{ $data->fakultas }}</option>
						      @endforeach
						    </select>
							</div>
						<div class="col-md-6">
							<p>Angkatan : </p>
							<input type="text" required="" class="form-control"  name="angkatan"  >
						</div>
						<div class="col-md-12">
							<br>
							<button type="submit" class="btn btn-primary">save</button>
						</div>

					</form>
				</div>

		</div>
	</div>
</div>
</div>
@endsection
