@extends('layouts.app')
@section('title','Staff BEMKM')
@section('content')

<div class="container">
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"> Edit Staff BEMKM
				</h3>
				<div class="panel-body">
					<form class="form-horizontal" action="{{url('staf_bem/update/'.$staf_bem->id) }}" method="post" enctype="multipart/form-data" files=true>
						{{csrf_field()}}
						<div class="col-md-6">
							<p>Nama : </p>
							<input type="text" value="{{ $staf_bem->nama }}" class="form-control"  name="nama"  >
						</div>
						<div class="col-md-6">
							<p>Fakultas : </p>
						<select class="form-control"   name="fakultas_id">
						      <option>.....</option>
						      @foreach ($fakultas as $data)
						      	{{-- expr --}}
								<option value="{{ $data->id }}" @if($staf_bem->fakultas_id == $data->id) selected="selected"@endif>{{ $data->fakultas }} </option>
						      @endforeach

						    </select>
							</div>
						<div class="col-md-6">
							<p>Angkatan : </p>
							<input type="text" value="{{ $staf_bem->angkatan }}" class="form-control"  name="angkatan"  >
						</div>
						<div class="col-md-12">
							<br>
							<button type="submit" class="btn btn-primary">save</button>
						</div>
					</form>
				</div>

		</div>
	</div>
</div>
</div>
@endsection