@extends('layouts.app')
@section('title','Slide')
@section('content')

<div class="container">
	@if (Session::has('message'))
				<div class="alert alert-info">{{ Session::get('message') }}</div>
				@endif
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"> Edit Slide
				</h3>
				<div class="panel-body">
					<form class="form-horizontal" action="{{url('slide/edit/'.$slide->id) }}" method="post" enctype="multipart/form-data" files=true>
						{{csrf_field()}}
						<div class="col-md-6">
							<p>Nama : </p>
							<input type="text" required="" class="form-control"  name="judul" value="{{ $slide->judul }}" >
							</div>

						<div class="col-md-6">
							<p>Picture : </p>
							<input type="file"  class="form-control" name="file" value="" >
							<br>

									<img src="{{ asset('images/slide/'.$slide->foto) }}" style="height:50px;width:10%;object-fit:cover;" alt="">

						</div>
						<div class="col-md-6">
							<p>Deskripsi : </p>
							<textarea type="text" required="" class="form-control" name="deskripsi" >{{ $slide->deskripsi }}</textarea>
							</div>


						<div class="col-md-12">
							<br>
							<button type="submit" class="btn btn-primary">save</button>
						</div>
					</form>
				</div>

		</div>
	</div>
</div>
</div>
@endsection