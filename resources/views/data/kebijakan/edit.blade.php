@extends('layouts.app')
@section('title','Kebijakan')
@section('content')

<div class="container">
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"> Edit Kebijakan
				</h3>
				<div class="panel-body">
					<form class="form-horizontal" action="{{url('kebijakan/edit/'.$kebijakan->id) }}" method="post" enctype="multipart/form-data" files=true>
						{{csrf_field()}}
						<div class="col-md-6">
							<p>Kebijakan : </p>
							<input type="text" required="" class="form-control"  name="nama" value="{{ $kebijakan->nama }}" >
							</div>


						<div class="col-md-12">
							<br>
							<button type="submit" class="btn btn-primary">save</button>
						</div>
					</form>
				</div>

		</div>
	</div>
</div>
</div>
@endsection