@extends('layouts.app')
@section('title','On Going')
@section('content')
	  <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
	  <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
	  <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
	  <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
<div class="container">
	@if (Session::has('message'))
				<div class="alert alert-info">{{ Session::get('message') }}</div>
				@endif
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"> Edit On Going
				</h3>
				<div class="panel-body">
					<form class="form-horizontal" action="{{url('going/edit/'.$going->id) }}" method="post" enctype="multipart/form-data" files=true>
						{{csrf_field()}}
						<div class="col-md-6">
							<p>Nama : </p>
							<input type="text" required="" class="form-control"  name="judul" value="{{ $going->judul }}" >
							</div>

						<div class="col-md-6">
							<p>Picture : </p>
							<input type="file"  class="form-control" name="file" value="" >
							<br>

									<img src="{{ asset('images/going/'.$going->foto) }}" style="height:50px;width:10%;object-fit:cover;" alt="">

						</div>
						<div class="col-md-6">
							<p>Deskripsi : </p>
							<textarea type="text" id="coba" required="" class="form-control" name="deskripsi" >{!! $going->deskripsi !!}</textarea>
							</div>
							<div class="col-md-6">
						<div class="col-md-12">
							<br>
							<button type="submit" class="btn btn-primary">save</button>
						</div>
					</form>
				</div>

		</div>
	</div>
</div>
</div>
<script>
$('#coba').summernote({
  toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['fontstyle',['fontname']],
    ['fontsize', ['fontsize']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']],
    ['insert',['link']],
    ['misc',['codeview']]
  ],
fontNames: [
    'Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
    'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande'
  ],fontNamesIgnoreCheck: [
    'Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
    'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande'
  ]
});
$('.dropdown-toggle').dropdown()
</script>
@endsection