@extends('layouts.app')
@section('title','On Going')
@section('content')
    	<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
@if (Session::has('message'))
				<div class="alert alert-info">{{ Session::get('message') }}</div>
				@endif
					<div class="box-header with-border">
						<h3 class="box-title"> Table On Going <a href="{{ url('going/create') }}" style="float: right;" class="btn btn-success" > <i class="fa fa-plus"></i></a>
				</h3>
			</div>

				<div class="box-body">
					<table class="table table-responsive table-hover">
						<thead>
							<tr>
								<th>No</th>
								<th>Judul</th>
								<th>Deskripsi</th>
								<th width="40%" scope="row">Picture</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@php
								$no =1;
							@endphp
							@foreach ($going as $data)
							<tr>
								<td>{{$no++}}</td>
								<td>{{$data->judul or 'Kosong'}}</td>
								<td>{!!$data->deskripsi or 'Kosong'!!}</td>
								<td>

									<img src="{{ asset('images/going/'.$data->foto) }}" style="height:50px;width:10%;object-fit:cover;" alt="">

								</td>
								<td>
									<div class="modal fade" id="hapus" tabindex="-1" role="dialog" aria-labelledby="Terms and conditions" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
													<h3 class="modal-title">Apa anda yakin ingin menghapus?</h3>
												</div>

												<div class="modal-footer delete2" id="delete">
									</div>

											</div>
										</div>
									</div>
									<div class="dropdown">
										<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">option
											<span class="caret"></span></button>
											<ul class="dropdown-menu">
												<li><a    href="{{url('going/edit/'.$data->id)}}">Edit</a></li>

												<li><a data-toggle="modal" data-target="#hapus" onclick="modalTimbul({{ $data->id }})" href="#">Delete</a></li>
											</ul>
										</div>
									{{-- <a href="#" class="btn btn-default">View</a>
									<a href="{{url('admin/going/edit/'.$data->id)}}" class="btn btn-warning">Edit</a>
									<a href="" class="btn btn-danger">Delete</a> --}}
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				{!! $going->render() !!}

		</div>
	</div>
</div>
<script>
		function modalTimbul(id) {
			$.ajax({
				url : "../deletegoing/"+id,

			}).done(function(data){
				$(".delete2").html(data);
			});
		}
	</script>
@endsection


