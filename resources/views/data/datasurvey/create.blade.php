@extends('layouts.app')
@section('title','Data Survey')
@section('content')
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
	  <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
	  <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
	  <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"> Input Data Survey
				</h3>
				<div class="panel-body">
					<form class="form-horizontal" action="{{ url('datasurvey/store') }}" method="post" enctype="multipart/form-data" files=true>
						{{csrf_field()}}
						<div class="col-md-3">
							<p>Kategori : </p>
							<select required="" class="form-control"   name="kategori_id">
						      <option>.....</option>
						      @foreach ($kategori as $data)
						      <option value="{{ $data->id }}">{{ $data->nama }}</option>
						      @endforeach
						    </select>
							</div>
							<div class="col-md-5">
							<p>Judul : </p>
							<input type="text" required="" class="form-control" name="judul" value="" >
							<br>
							<br>
							</div>
						<div class="col-md-4">
							<p>Picture : </p>
							<input type="file" required="" class="form-control" name="file" value="" >

						</div>
							<div class="col-md-12">
							<p>Deskripsi : </p>
							<textarea id="coba" type="text" required="" class="form-control" name="deskripsi" value="" ></textarea>
							</div>


						<div class="col-md-12">
							<br>
							<button type="submit" class="btn btn-primary">save</button>
						</div>
					</form>
				</div>

		</div>
	</div>
</div>
</div>
<script>
$('#coba').summernote({
	 height: 250,                 // set editor height
  minHeight: null,             // set minimum height of editor
  maxHeight: null,             // set maximum height of editor
  focus: true,                 // set focus to editable area after initializing summernote
  toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['fontstyle',['fontname']],
    ['fontsize', ['fontsize']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']],
    ['insert',['link']],
    ['misc',['codeview']]
  ],
fontNames: [
    'Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
    'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande'
  ],fontNamesIgnoreCheck: [
    'Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
    'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande'
  ]
});
$('.dropdown-toggle').dropdown()
</script>
@endsection