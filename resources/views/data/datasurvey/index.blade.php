@extends('layouts.app')
@section('title','Data Survey')
@section('content')
		
		<!-- Search -->
		<div class="container search-admin">
					<div class="row" style="float: right; margin-right: 20px;">
						<form class="input-group" action="{{ url('admin/searching') }}" method="get">
						<div class="col-md-6">
							<input type="text" class="form-control mr-2" name="q" placeholder="Cari Data ...">
						</div>
						<div class="col-md-5" style="padding: 0;">
			                <span><select class="form-control mr-2" name="kategori" id="exampleSelect1" style="width: 95%;">
			                        <option value="semua" >Semua Kategori</option>
			                        @foreach ($kategori as $data)
			                        	<option value="{{ $data->id }}" >{{ $data->nama}}</option>
			                        @endforeach
			                       </select></span>
			          </div>

			          <div class="col-md-1" style="padding: 0;">
			          	<span class="input-group-btn">
			                  <button class="btn btn-secondary mr-2" type="submit"><i class="fa fa-search"></i></button>
			                </span> 
			          </div>

			          </form>
					</div>
				</div>
				<!-- Search END-->

    	<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
@if (Session::has('message'))
				<div class="alert alert-info">{{ Session::get('message') }}</div>
				@endif
					<div class="box-header with-border">
						<h3 class="box-title"> Table Data Survey <a href="{{ url('datasurvey/create') }}" style="float: right;" class="btn btn-success" > <i class="fa fa-plus"></i></a>
				</h3>
			</div>

				<div class="box-body">
					<table class="table table-responsive table-hover">
						<thead>
							<tr>
								<th>No</th>
								<th>Kategori</th>
								<th width="20%">Judul</th>
								<th width="40%">Deskripsi</th>
								<th width="20%" scope="row">Picture</th>
								<th>Model</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@php
								$no =1;
							@endphp
							@foreach ($datasurvey as $data)
							<tr>
								<td>{{$no++}}</td>
								<td>{{$data->kategori->nama or 'kosong'}}</td>
								<td>{{$data->judul or 'Kosong'}}</td>
								<td><div style="max-height:100px; overflow:hidden; text-overflow:ellipsis;">{!!$data->deskripsi or 'Kosong'!!}</div></td>
								<td>

									<img src="{{ asset('images/datasurvey/'.$data->foto) }}" style="height:50px;width:10%;object-fit:cover;" alt="">

								</td>
								<td>{{$data->kategori->model or 'Kosong'}}</td>
								<td>
									<div class="modal fade" id="hapus" tabindex="-1" role="dialog" aria-labelledby="Terms and conditions" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
													<h3 class="modal-title">Apa anda yakin ingin menghapus?</h3>
												</div>

												<div class="modal-footer delete2" id="delete">
									</div>

											</div>
										</div>
									</div>
									<div class="dropdown">
										<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">option
											<span class="caret"></span></button>
											<ul class="dropdown-menu">
												<li><a    href="{{url('datasurvey/edit/'.$data->id)}}">Edit</a></li>

												<li><a data-toggle="modal" data-target="#hapus" onclick="modalTimbul({{ $data->id }})" href="#">Delete</a></li>
											</ul>
										</div>
									{{-- <a href="#" class="btn btn-default">View</a>
									<a href="{{url('admin/datasurvey/edit/'.$data->id)}}" class="btn btn-warning">Edit</a>
									<a href="" class="btn btn-danger">Delete</a> --}}
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				{!! $datasurvey->render() !!}

		</div>
	</div>
</div>
<script>
		function modalTimbul(id) {
			$.ajax({
				url : "../deletedatasurvey/"+id,

			}).done(function(data){
				$(".delete2").html(data);
			});
		}
	</script>
@endsection


