<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
	<title>BEMKM</title>
	<link href="https://fonts.googleapis.com/css?family=Raleway:700" rel="stylesheet">
	<link href="{{ asset('css/materialize_2.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
	<link href="{{ asset('css/style-home.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>

	<style type="text/css">
		body {
			font-family: 'Raleway', sans-serif;
		}

		h1 {
			font-size: 13em;
			margin: 0.4em 0 0 0;
			line-height: 1;
		}

		h2 {
			font-size: 1em;
			margin: 25px 0 10 0;
			text-transform: capitalize;
		}

		a {
			margin: 25px 0 10px 0;
			width: 250px;
		}

		img {
			margin: 5px;
			width: 40px;
			height: 40px;
		}
	</style>
</head>
<body>

	@yield('content')

	@yield('js')
</body>
</html>

