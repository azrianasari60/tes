<div class="row top_tiles">
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-bullhorn"></i></div>
            <div class="count">{{ count(App\pengaduan::all()) }}</div>
            <h3><a href="#">Pengaduan</a></h3>
        </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-newspaper-o"></i></div>
            <div class="count">{{ count(App\Berita::all()) }}</div>
            <h3><a href="#">Berita</a></h3>
        </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-file-text-o "></i></div>
            <div class="count">{{ count(App\Survey::all()) }}</div>
            <h3><a href="#">Kumpulan Survey</a></h3>
        </div>
    </div>
</div>