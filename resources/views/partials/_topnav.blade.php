<div class="top_nav">
    <div class="nav_menu">
        <nav class="" role="navigation">
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img src="/images/img.jpg" alt="">{{auth()->user()->name}}
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="{{url('user/edit/'.Auth::id())}}">Profil</a></li>
                        <li><a href="{{url('/logout')}}" onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                                <i class="fa fa-sign-out pull-right"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
                <style>
ul.scroll {

    height: 500px;
    overflow: scroll;
}

</style>
                <li role="presentation" class="dropdown notifications-menu" id="markasread"  >
                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-envelope-o"></i>
                        <span class="badge bg-green">{{auth()->user()->unreadNotifications->count()}}</span>
                    </a>
                    <ul id="menu1" class="dropdown-menu list-unstyled msg_list {{ count(auth()->user()->unreadNotifications) >= 12 ? 'scroll' : '' }}" role="menu" >
                    @if(count(auth()->user()->unreadNotifications) == 0)
                    <li>Tidak Ada Notification </li>
                    @else
                        @foreach (auth()->user()->unreadNotifications as $notification )
                        <li>  @include('admin.notification.'.snake_case(class_basename($notification->type)))</li>
                        @endforeach
                        <li>
                            <div class="text-center">
                                <a href="{{ url('admin/pengaduan') }}" onclick="markNotificationAsRead({{auth()->user()->unreadNotifications->count()}})">
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                             @endif

                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>
