<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{{url('Dashboard')}}" class="site_title"><span>Bem KM</span></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile">
            <div class="profile_pic">
                <img src="/images/img_1613.jpg" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Welcome,</span>
                <h2>{{auth()->user()->name}}</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                     @if(Auth::user()->status != 'Bank Data')

                    <li><a href="{{url('admin/user')}}"><i class="fa fa-user"></i> User</a></li>
                    <li><a href="{{route('dashboard')}}"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><a href="{{ url('admin/berita') }}"><i class="fa fa-newspaper-o "></i> Berita</a></li>
                    <li><a href="{{ url('admin/pengaduan') }}"><i class="fa fa-bullhorn"></i> Pengaduan</a></li>
                    <li><a href="{{route('slideshow')}}"><i class="fa fa-caret-square-o-right "></i> Slideshow</a></li>
                    <li><a><i class="fa fa-picture-o"></i> Gambar <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="{{ url('admin/banner') }}">Banner Berita</a></li>
                          <li><a href="{{ url('admin/logo') }}">Logo Kabinet</a></li>
                          <li><a href="{{ url('admin/filsuf') }}">Filosofi Logo</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-university "></i> Tentang Bem KM<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ url('admin/tentang') }}">Tentang BEMKM</a></li>
                            <li><a href="{{ url('admin/kontak') }}">Kontak</a></li>
                            <li><a href="{{ url('admin/program') }}">Program Unggulan</a></li>
                            <li><a href="{{ url('admin/visi') }}">Visi</a></li>
                            <li><a href="{{ url('admin/misi') }}">Misi</a></li>
                            <li><a href="{{ url('admin/struktur') }}">Struktur Kepengurusan</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ url('admin/biro') }}"><i class="fa fa-users"></i> Biro / Kementrian</a>
                        {{-- <ul class="nav child_menu">
                            <li><a href="{{ url('admin/biro') }}">Biro</a></li>
                            <li><a href="{{ url('admin/kementrian') }}">Kementrian</a></li>
                        </ul></li> --}}
                        @endif
                    <li><a href="#"><i class="fa fa-database"></i> Bank Data<span class="fa fa-chevron-down"></span></a>
                     <ul class="nav child_menu">
                        <li><a href="#"></i> Data Depan<span class="fa fa-chevron-down"></span></a>
                     <ul class="nav child_menu">
                        <li><a href="{{ url('admin/slide') }}">Slide</a></li>
                        <li><a href="{{ url('admin/kebijakan') }}">Kebijakan</a></li>
                     </ul>
                 </li>
                 <li><a href="#"></i> DataBase<span class="fa fa-chevron-down"></span></a>
                     <ul class="nav child_menu">
                        <li><a href="{{ url('admin/ukm') }}">UKM</a></li>
                        <li><a href="{{ url('admin/fakultas') }}">Fakultas</a></li>
                        <li><a href="{{ url('admin/staf') }}">Staff BEM</a></li>
                     </ul>
                 </li>
                            <li><a href="{{ url('admin/kontakdata') }}">Kontak</a></li>
                            <li><a href="{{ url('admin/going') }}">On Going</a></li>
                            <li><a href="{{ url('admin/kategori') }}">Kategori</a></li>
                            <li><a href="{{ url('admin/datasurvey') }}">DataSurvey</a></li>
                        </ul></li>
                </ul>
            </div>

        </div>
        <!-- /sidebar menu -->

        @include('partials._sidenav_footer')
    </div>
</div>
