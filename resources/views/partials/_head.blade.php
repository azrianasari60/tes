<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title') </title>

    <!-- App Css -->
    <link href="{{asset('css/app.css')}}" rel="stylesheet">

<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59fa9249f809021c"></script>

    @stack('header-scripts')

</head>