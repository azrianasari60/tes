@extends('layouts.app')
@section('title','Banner Berita')
@section('content')

<div class="container">
	@if (Session::has('message'))
				<div class="alert alert-info">{{ Session::get('message') }}</div>
				@endif
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"> Edit Banner Berita
				</h3>
				<div class="panel-body">
					<form class="form-horizontal" action="{{url('banner/edit/') }}" method="post" enctype="multipart/form-data" files=true>
						{{csrf_field()}}

						<div class="col-md-6">
							<p>Picture (1360px x 200px): </p>
							<input type="file" class="form-control" name="file" value="" multiple="">
							<br>

									<img src="{{ asset('images/banner.jpg') }}" style="height:50px;width:10%;object-fit:cover;" alt="">

						</div>


						<div class="col-md-12">
							<br>
							<button type="submit" class="btn btn-primary">save</button>
						</div>
					</form>
				</div>

		</div>
	</div>
</div>
</div>
@endsection
