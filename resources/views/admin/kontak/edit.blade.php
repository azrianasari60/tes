@extends('layouts.app')
@section('title','Kontak')
@section('content')

<div class="container">
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"> Edit Kontak
				</h3>
				<div class="panel-body">
					<form class="form-horizontal" action="{{url('kontak/update/'.$kontak->id) }}" method="post" enctype="multipart/form-data" files=true>
						{{csrf_field()}}
						<div class="col-md-6">
							<p>Alamat : </p>
							<input type="text" required="" class="form-control" value="{{ $kontak->alamat }}" name="alamat"  >
							</div>
						<div class="col-md-6">
							<p>Id Line : </p>
							<input type="text" required="" class="form-control" value="{{ $kontak->line }}" name="line"  >
							</div>
						<div class="col-md-6">
							<p>No Hp 1 : </p>
							<input type="text" required="" class="form-control" value="{{ $kontak->no_hp }}" name="no_hp"  >
							</div>
						<div class="col-md-6">
							<p>No Hp 2 : </p>
							<input type="text" required="" class="form-control" value="{{ $kontak->no_telp }}" name="no_telp"  >
							</div>
						<div class="col-md-12">
							<br>
							<button type="submit" class="btn btn-primary">save</button>
						</div>
					</form>
				</div>

		</div>
	</div>
</div>
</div>
@endsection
