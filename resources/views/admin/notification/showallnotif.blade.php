@extends('layouts.app')
@section('title','Pengadu')
@section('content')
    	<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
@if (Session::has('message'))
				<div class="alert alert-info">{{ Session::get('message') }}</div>
				@endif
					<div class="box-header with-border">
						<h3 class="box-title">Pengadu
				</h3>
			</div>

				<div class="box-body">
					<table class="table table-responsive table-hover">
						<thead>
							<tr>
								<th>No</th>
								<th>Pengadu</th>
							</tr>
						</thead>
						<tbody>
							@php
								$no =1;
							@endphp
						  @foreach (auth()->user()->unreadNotifications as $notification )
							<tr>
								<td>{{$no++}}</td>
                <td>
                <a href="{{ url('pengaduan/show/'.$notification->data['user']['id'].'/'.$notification->id)}}">
                	{{$notification->data['user']['email']}} Mengadu</a>
                </td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>


		</div>
	</div>
</div>
<script>
		function modalTimbul(id) {
			$.ajax({
				url : "../deleteprogram/"+id,

			}).done(function(data){
				$(".delete2").html(data);
			});
		}
	</script>
@endsection
