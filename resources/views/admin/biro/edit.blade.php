@extends('layouts.app')
@section('title','Biro')
@section('content')
<script src="{{asset('js/jquery.min.js')}}"></script>
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"> Edit Biro
                </h3>
                <div class="panel-body">
                    <form class="form-horizontal" action="{{ url('biro/update/'.$biro->id) }}" method="post" enctype="multipart/form-data" files=true>
                        {{csrf_field()}}
                        <div class="col-md-12">
                            <p>Nama Biro: </p>
                            <input type="text" required="" class="form-control"  name="nama" value="{{ $biro->nama }}" >
                            </div>
                            <div class="col-md-6">
                              <p>Logo : </p>
                              <input type="file" id="logoToUpload" class="form-control" name="logo" value="">
                              <img id="lihat" style="margin-bottom: 5px;" src="{{ asset('images/biro/'.$biro->logo) }}" height="100" alt="">
                              <br>

                            </div>
                        <div class="col-md-6">
                            <p>Picture : </p>
                            <input type="file" class="form-control" id="fileToUpload" name="file">
                              <img id="preview" style="margin-bottom: 5px;" src="{{ asset('images/biro/'.$biro->foto) }}" height="100" alt="">
                        </div>
                        <br>
                        <br><br>
                        <div class="col-md-12">
 <table id="purchaseItems" align="center" class="table table-bordered">
    <tr>
        <td style="text-align: center;">Nama Proker</td>
        <td style="text-align: center;">Deskripsi</td>

    </tr>
    @foreach ($biro->proker as $key => $data)
    <tr>



        <td>
            <input type="text" name="judul[]" class="form-control" value="{{ $data->judul }}"  />
        </td>
        <td>
            <textarea type="text" name="deskripsi[]" class="form-control">{{ $data->deskripsi }} </textarea>
        </td>
         <td>
            <input type="button" name="addRow[]" id="" class="add btn btn-primary btn-sm" value='+' />
        </td>
        <td>
            <input type="button" name="addRow[]" id="" class="removeRow btn btn-danger btn-sm" value='-' />
        </td>
    </tr>
  @endforeach
</table>
</div>


                        <div class="col-md-12">
                            <br>
                            <button type="submit" class="btn btn-primary">save</button>
                        </div>
                    </form>
                </div>

        </div>
    </div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

    $(document).on('click', '#purchaseItems .add', function () {

        var row = $(this).closest('tr');
        var clone = row.clone();

        // clear the values
        var tr = clone.closest('tr');
        tr.find('input[type=text]').val('');

        $(this).closest('tr').after(clone);

    });

    $(document).on('keypress', '#purchaseItems .next', function (e) {
        if (e.which == 13) {
            var v = $(this).index('input:text');
            var n = v + 1;
            $('input:text').eq(n).focus();
            //$(this).next().focus();
        }
    });
    $(document).on('keypress', '#purchaseItems .nextRow', function (e) {
        if (e.which == 13) {
            $(this).closest('tr').find('.add').trigger('click');
            $(this).closest('tr').next().find('input:first').focus();

        }
    });
    $(document).on('click', '#purchaseItems .removeRow', function () {
        if ($('#purchaseItems .add').length > 1) {
            $(this).closest('tr').remove();
        }
    });
});
</script>
<script>
$(function()
{
   $("#fileToUpload").on('change', function(){
   // Display image on the page for viewing
        readURL(this,"preview");

   });
});

function readURL(input , tar) {
	if (input.files && input.files[0]) { // got sth

    // Clear image container
    $("#" + tar ).removeAttr('src');

    $.each(input.files , function(index,ff)  // loop each image
    {

        var reader = new FileReader();

        // Put image in created image tags
        reader.onload = function (e) {
            $('#' + tar).attr('src', e.target.result);
        }

        reader.readAsDataURL(ff);

    });
	}
}

</script>
<script>
$(function()
{
   $("#logoToUpload").on('change', function(){
   // Display image on the page for viewing
        readURL(this,"lihat");

   });
});

function readURL(input , tar) {
	if (input.files && input.files[0]) { // got sth

    // Clear image container
    $("#" + tar ).removeAttr('src');

    $.each(input.files , function(index,ff)  // loop each image
    {

        var reader = new FileReader();

        // Put image in created image tags
        reader.onload = function (e) {
            $('#' + tar).attr('src', e.target.result);
        }

        reader.readAsDataURL(ff);

    });
	}
}

</script>
@endsection
