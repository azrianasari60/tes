@extends('layouts.app')
@section('title','Visi')
@section('content')

<div class="container-fluid spark-screen">
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"> Input Visi
				</h3>
				<div class="panel-body">
					<form class="form-horizontal" action="{{ url('visi/store') }}" method="post" enctype="multipart/form-data" files=true>
						{{csrf_field()}}
						<div class="col-md-6">
							<p>Visi : </p>
							<input type="text" required="" class="form-control" name="visi" value="" >
							</div>



						<div class="col-md-12">
							<br>
							<button type="submit" class="btn btn-primary">save</button>
						</div>
					</form>
				</div>

		</div>
	</div>
</div>
</div>
@endsection