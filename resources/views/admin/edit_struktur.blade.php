@extends('layouts.app')
@section('title','Struktrur Kepengurusan')
@section('content')

<div class="container">
	@if (Session::has('message'))
				<div class="alert alert-info">{{ Session::get('message') }}</div>
				@endif
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"> Edit Struktrur Kepengurusan
				</h3>
				<div class="panel-body">
					<form class="form-horizontal" action="{{url('struktur/edit/') }}" method="post" enctype="multipart/form-data" files=true>
						{{csrf_field()}}

						<div class="col-md-6">
							<p>Picture (min: 1366px x 660px): </p>
							<input type="file"  class="form-control" name="file" value="" multiple="">
							<br>

									<img src="{{ asset('images/struktur.jpg') }}" style="height:50px;width:10%;object-fit:cover;" alt="">

						</div>


						<div class="col-md-12">
							<br>
							<button type="submit" class="btn btn-primary">save</button>
						</div>
					</form>
				</div>

		</div>
	</div>
</div>
</div>
@endsection