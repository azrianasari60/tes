@extends('layouts.app')
@section('title','User')
@section('content')

<div class="container">
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"> Input User
				</h3>
				<div class="panel-body">
					<form class="form-horizontal" action="{{url('user/store/') }}" method="post" enctype="multipart/form-data" files=true>
						{{csrf_field()}}
						<div class="col-md-6">
							<p>Nama : </p>
							<input type="text" required="" class="form-control"  name="name"  >
							</div>

							<div class="col-md-6">
							<p>Email : </p>
							<input type="text" required="" class="form-control"  name="email"  >
							</div>
						<div class="col-md-6">
							<p>Password : </p>
							<input type="text" required="" class="form-control"  name="password"  >
							</div>
						<div class="col-md-6">
							<p>Status : </p>
							<select class="form-control"   name="status">
						      <option>.....</option>
						      <option>Admin</option>
						      <option>Bank Data</option>
						  </select>
						</div>
						<div class="col-md-12">
							<br>
							<button type="submit" class="btn btn-primary">save</button>
						</div>
					</form>
				</div>

		</div>
	</div>
</div>
</div>
@endsection
