@extends('layouts.app')
@section('title','Slideshow')
@section('content')

<div class="container">
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"> Edit Slideshow
				</h3>
				<div class="panel-body">
					<form class="form-horizontal" action="{{url('slideshow/edit/'.$slideshow->id) }}" method="post" enctype="multipart/form-data" files=true>
						{{csrf_field()}}
						<div class="col-md-6">
							<p>Nama : </p>
							<input type="text" required="" class="form-control"  name="nama" value="{{ $slideshow->nama }}" >
							</div>
						<div class="col-md-6">
							<p>Salam : </p>
							<input type="text" required="" class="form-control" name="salam" value="{{ $slideshow->salam }}" >
							</div>
						<div class="col-md-6">
							<p>Tag Line : </p>
							<input type="text" required="" class="form-control" name="tag" value="{{ $slideshow->tag }}" >
							</div>
						<div class="col-md-6">
							<p>Tombol : </p>
							<input type="text" required="" class="form-control" name="tombol" value="{{ $slideshow->tombol }}" >
							</div>
						<div class="col-md-6">
							<p>Picture : </p>
							<input type="file"  class="form-control" name="file[]" value="" multiple="" id="files">
							<br>
							<div id="selectedFiles">

									@forelse ($slideshow->picture as $picture)
									<img id="preview" src="{{ asset('images/slideshow/'.$picture->picture) }}" style="height:50px;width:10%;object-fit:cover;" alt="">
									@empty
									Data Foto Kosong
									@endforelse
								</div>
						</div>


						<div class="col-md-12">
							<br>
							<button type="submit" class="btn btn-primary">save</button>
						</div>
					</form>
				</div>

		</div>
	</div>
</div>
</div>
	<script>
	var selDiv = "";

	document.addEventListener("DOMContentLoaded", init, false);

	function init() {
		document.querySelector('#files').addEventListener('change', handleFileSelect, false);
		selDiv = document.querySelector("#selectedFiles");
	}

	function handleFileSelect(e) {

		if(!e.target.files || !window.FileReader) return;

		selDiv.innerHTML = "";

		var files = e.target.files;
		var filesArr = Array.prototype.slice.call(files);
		filesArr.forEach(function(f) {
			if(!f.type.match("image.*")) {
				return;
			}

			var reader = new FileReader();
			reader.onload = function (e) {
				var html = "<img style='margin-bottom: 3px;' height='60'  src=\"" + e.target.result + "\">";
				selDiv.innerHTML += html;
			}
			reader.readAsDataURL(f);

		});


	}
	</script>
@endsection
