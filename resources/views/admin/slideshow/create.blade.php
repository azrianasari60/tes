@extends('layouts.app')
@section('title','Slideshow')
@section('content')

<div class="container-fluid spark-screen">
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"> Input Slideshow
				</h3>
				<div class="panel-body">
					<form class="form-horizontal" action="{{route('slideshow_store') }}" method="post" enctype="multipart/form-data" files=true>
						{{csrf_field()}}
						<div class="col-md-6">
							<p>Nama : </p>
							<input type="text" required="" class="form-control" name="nama" value="" >
							</div>
						<div class="col-md-6">
							<p>Salam : </p>
							<input type="text" required="" class="form-control" name="salam" value="" >
							</div>
						<div class="col-md-6">
							<p>Tag Line : </p>
							<input type="text" required="" class="form-control" name="tag" value="" >
							</div>
						<div class="col-md-6">
							<p>Tombol : </p>
							<input type="text" required="" class="form-control" name="tombol" value="" >
							</div>
						<div class="col-md-6">
							<p>Picture : </p>
							<input type="file" required="" class="form-control" id="files" name="file[]" value="" multiple="">
							<small> nb : gambar sebaiknya hanya 3</small>
						</div>
						<div id="selectedFiles">


								<img id="preview" src="" style="height:50px;width:10%;object-fit:cover;" alt="">

							</div>


						<div class="col-md-12">
							<br>
							<button type="submit" class="btn btn-primary">save</button>
						</div>
					</form>
				</div>

		</div>
	</div>
</div>
</div>
<script>
var selDiv = "";

document.addEventListener("DOMContentLoaded", init, false);

function init() {
	document.querySelector('#files').addEventListener('change', handleFileSelect, false);
	selDiv = document.querySelector("#selectedFiles");
}

function handleFileSelect(e) {

	if(!e.target.files || !window.FileReader) return;

	selDiv.innerHTML = "";

	var files = e.target.files;
	var filesArr = Array.prototype.slice.call(files);
	filesArr.forEach(function(f) {
		if(!f.type.match("image.*")) {
			return;
		}

		var reader = new FileReader();
		reader.onload = function (e) {
			var html = "<img style='margin-bottom: 3px;' height='60'  src=\"" + e.target.result + "\">";
			selDiv.innerHTML += html;
		}
		reader.readAsDataURL(f);

	});


}
</script>
@endsection
