@extends('layouts.app')
@section('title','Berita')
@section('content')
<script src="{{asset('js/jquery.min.js')}}"></script>
<div class="container">
	@if (Session::has('message'))
				<div class="alert alert-info">{{ Session::get('message') }}</div>
				@endif
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Berita
				</h3>
				<div class="panel-body">
					<form class="form-horizontal" action="{{url('berita/edit/'.$berita->id) }}" method="post" enctype="multipart/form-data" files=true>
						{{csrf_field()}}
						<div class="col-md-6">
							<p>Nama : </p>
							<input type="text"  readonly="" class="form-control"  name="judul" value="{{ $berita->judul }}" >
							</div>

						<div class="col-md-6">
							<p>Picture : </p>

							<br>
									@forelse ($berita->picture as $picture)
									<img id="preview" src="{{ asset('images/berita/'.$picture->picture) }}" style="height:50px;width:10%;object-fit:cover;" alt="">
									@empty
									Data Foto Kosong
									@endforelse
						</div>
						<div class="col-md-12">
							<p>Deskripsi : </p>
							<div>{!! $berita->deskripsi !!}</div>
							</div>

					</form>
				</div>

		</div>
	</div>
</div>
</div>
@endsection
