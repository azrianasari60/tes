@extends('layouts.app')
@section('title','Berita')
@section('content')
	<!-- <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet"> -->
	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
	<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
	<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
<div class="container">
	@if (Session::has('message'))
				<div class="alert alert-info">{{ Session::get('message') }}</div>
				@endif
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"> Edit Berita
				</h3>
				<div class="panel-body">
					<form class="form-horizontal" action="{{url('berita/edit/'.$berita->id) }}" method="post" enctype="multipart/form-data" files=true>
						{{csrf_field()}}
						<div class="col-md-4">
							<p>Nama : </p>
							<input type="text"  required="" class="form-control"  name="judul" value="{{ $berita->judul }}" >
							</div>

						<div class="col-md-4">
							<p>Picture : </p>
							<input type="file" id="fileToUpload" class="form-control" name="file" value="">
								</div>
							<div class="col-md-4" >
								<br>
									@forelse ($berita->picture as $picture)
									<img id="preview" src="{{ asset('images/berita/'.$picture->picture) }}" style="height:50px;width:20%;object-fit:cover;" alt="">
									@empty
									Data Foto Kosong
									@endforelse
						</div>
					<div class="col-md-12">
						<p>Deskripsi : </p>
						<textarea type="text" required="" class="form-control" name="deskripsi" id="coba">{!!$berita->deskripsi !!}</textarea>
						</div>


						<div class="col-md-12">
							<br>
							<button type="submit" class="btn btn-primary">save</button>
						</div>
					</form>
				</div>

		</div>
	</div>
</div>
</div>
<script type="text/javascript">
$(function()
{
   $("#fileToUpload").on('change', function(){
   // Display image on the page for viewing
        readURL(this,"preview");

   });
});

function readURL(input , tar) {
	if (input.files && input.files[0]) { // got sth

    // Clear image container
    $("#" + tar ).removeAttr('src');

    $.each(input.files , function(index,ff)  // loop each image
    {

        var reader = new FileReader();

        // Put image in created image tags
        reader.onload = function (e) {
            $('#' + tar).attr('src', e.target.result);
        }

        reader.readAsDataURL(ff);

    });
	}
}

</script>
<script>
$('#coba').summernote({
				 height: 250,                 // set editor height
  minHeight: null,             // set minimum height of editor
  maxHeight: null,             // set maximum height of editor
  focus: true,                 // set focus to editable area after initializing summernote

  toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['fontstyle',['fontname']],
    ['fontsize', ['fontsize']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']],
    ['insert',['link']],
    ['misc',['codeview']]
  ],
fontNames: [
    'Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
    'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande'
  ],fontNamesIgnoreCheck: [
    'Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
    'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande'
  ]
});
$('.dropdown-toggle').dropdown()
</script>
@endsection
