@extends('layouts.app')
@section('title','Berita')
@section('content')
    	<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
@if (Session::has('message'))
				<div class="alert alert-info">{{ Session::get('message') }}</div>
				@endif
					<div class="box-header with-border">
						<h3 class="box-title"> Table Berita <a href="{{ url('berita/create') }}" style="float: right;" class="btn btn-success"> <i class="fa fa-plus"></i></a>
				</h3>
			</div>

				<div class="box-body">
					<table class="table table-responsive table-hover">
						<thead>
							<tr>
								<th>No</th>
								<th width="30%" scope="row">Judul</th>
								<th width="30%" scope="row">Deskripsi</th>
								<th width="20%" scope="row">Picture</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@php
								$no =1;
							@endphp
							@foreach ($berita as $data)
							<tr>
								<td>{{$no++}}</td>
								<td><div style="max-height:100px; overflow:hidden; text-overflow:ellipsis;">{{$data->judul or 'Kosong'}}</div></td>
								<td><div style="max-height:100px; max-width: 300px; overflow:hidden; text-overflow:ellipsis;">{!!$data->deskripsi or 'Kosong'!!}</div></td>
								<td>
									@forelse ($data->picture as $picture)
									<img src="{{ asset('images/berita/'.$picture->picture) }}" style="height:50px;width:10%;object-fit:cover;" alt="">
									@empty
									Data Foto Kosong
									@endforelse
								</td>
								<td>
									<div class="modal fade" id="hapus" tabindex="-1" role="dialog" aria-labelledby="Terms and conditions" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
													<h3 class="modal-title">Apa anda yakin ingin menghapus?</h3>
												</div>

												<div class="modal-footer delete2" id="delete">
									</div>

											</div>
										</div>
									</div>
									<div class="dropdown">
										<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">option
											<span class="caret"></span></button>
											<ul class="dropdown-menu">
												<li><a    href="{{url('berita/show/'.$data->id)}}">Lihat</a></li>
												<li><a    href="{{url('berita/edit/'.$data->id)}}">Edit</a></li>

												<li><a data-toggle="modal" data-target="#hapus" onclick="modalTimbul({{ $data->id }})" href="#">Delete</a></li>
											</ul>
										</div>
									{{-- <a href="#" class="btn btn-default">View</a>
									<a href="{{url('admin/berita/edit/'.$data->id)}}" class="btn btn-warning">Edit</a>
									<a href="" class="btn btn-danger">Delete</a> --}}
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				{!! $berita->render() !!}

		</div>
	</div>
</div>
<script>
		function modalTimbul(id) {
			$.ajax({
				url : "../deleteberita/"+id,

			}).done(function(data){
				$(".delete2").html(data);
			});
		}
	</script>
@endsection
