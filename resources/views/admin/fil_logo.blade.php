@extends('layouts.app')
@section('title','Gambar')
@section('content')
    	<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
@if (Session::has('message'))
				<div class="alert alert-info">{{ Session::get('message') }}</div>
				@endif
					<div class="box-header with-border">
						<h3 class="box-title"> Table Gambar
				</h3>
			</div>

				<div class="box-body">
					<table class="table table-responsive table-hover">
						<thead>
							<tr>
								<th>No</th>
								<th>nama</th>
								<th width="40%" scope="row">Picture</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Filosofi Logo</td>
								<td>

									<img src="{{ asset('images/fil_logo.jpg') }}" style="height:50px;width:10%;object-fit:cover;" alt="">

								</td>
								<td>
									<div class="dropdown">
										<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">option
											<span class="caret"></span></button>
											<ul class="dropdown-menu">

												<li><a    href="{{ url('fil_logo/edit') }}">Edit</a></li>


											</ul>
										</div>
									{{-- <a href="#" class="btn btn-default">View</a>
									<a href="{{url('admin/berita/edit/'.$data->id)}}" class="btn btn-warning">Edit</a>
									<a href="" class="btn btn-danger">Delete</a> --}}
								</td>
							</tr>

						</tbody>
					</table>
				</div>
				{{-- {!! $berita->render() !!} --}}

		</div>
	</div>
</div>
@endsection


