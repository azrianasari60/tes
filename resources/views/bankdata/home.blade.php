<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="robots" content="all,follow">
    <title>BEM KM UNMUL | BANK DATA</title>
    <link rel="icon" type="image/png" href="assets/img/logo_bem.png">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/bootstrap/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/font-awesome/css/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">

    <!-- <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css"> -->
</head>

<body>

<header id="bankdata-header">
<div class="container-fluid">
  <div class="row">
    <div class="col-md-9">
    <nav class="navbar navbar-expand-md navbar-light bg-faded" style="z-index: 2; padding: 0;">
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="#"><img src="assets/img/logo_bem.png" style="height: 50px; top: 50%; transform: translateX(-50%);"></a>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav ml-auto" style=" margin: 0;">
      <a class="nav-item" href="#">Bank Data<span class="sr-only">(current)</span></a>
      <a class="nav-item" href="{{url('/')}}">Beranda</a>
      <a class="nav-item" href="{{url('berita')}}">Berita</a>
    </div>
  </div>
</nav>
    </div>

    <div class="col-lg-3 bankdata-search">
      <form class="input-group" action="{{ url('data/search') }}" method="get">
      <input type="text" class="form-control" placeholder="Search for..." name="q">
      <span class="input-group-btn">
        <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
      </span>

        </form>
    </div>
  </div>
</div>
</header>

@foreach ($slidebank as $data)
  {{-- expr --}}
<section id="bd_tentang" style="background-image: url({{asset('images/slide/'.$data->foto)}});">
      <div class="tentang-bd">
        <div class="text">
          <h5 class="text-center">{{ $data->judul}}</h5>
          <p class="center-justified">{{ $data->deskripsi or '' }}</p>
          </div>
      </div>
</section>
@endforeach

<section id="jelajahi">
    <h4>"Jelajahi dan Temukan Data yang Anda Butuhkan Dalam Aspek Kemahasiswaan Dengan Bebas"</h4>
</section>

<section id="kemahasiswaan">
  <div class="header">
  <h4>Kemahasiswaan</h4>
  </div>
  <div class="container-fluid">
    <div class="row">

@foreach ($kategori as $data)
  {{-- expr --}}
      <div class="col-md-3 col-sm-6">
        <a href="{{url('bankdata-index/'.$data->id)}}">
        <div class="card card-mhs">
        <!-- <img class="card-img-top" src="..." alt="Card image cap"> -->
        <div class="mhs-logo">
          <img src="{{asset('images/kategori/'.$data->foto)}}">
        </div>
        <div class="card-block">
          <h4 class="card-title text-center">{{ $data->nama }}</h4>
          <p class="card-text center-justified">{{ $data->deskripsi or ''}}</p>
        </div>
      </div>
      </a>
      </div>
@endforeach

    </div>
  </div>
</section>

<section id="pergerakan">
    <div class="header">
      <h4>Pergerakan</h4>
    </div>
    <div class="container">
      <div class="row">
        @foreach ($kategori1 as $data)
          {{-- expr --}}
      <div class="col-md-4">
        <a href="{{url('bankdata-index/'.$data->id)}}">
        <div class="card card-prg">
        <!-- <img class="card-img-top" src="..." alt="Card image cap"> -->
        <div class="prg-logo">
          <img src="{{asset('images/kategori/'.$data->foto)}}">
        </div>
        <div class="card-block">
          <h4 class="card-title text-center">{{ $data->nama }}</h4>
          <p class="card-text center-justified">{{ $data->deskripsi or ''}}</p>
        </div>
      </div>
      </a>
      </div>
        @endforeach

      </div>
    </div>
</section>

<section id="survey">
  <div class="container">
    <div class="row">

      <div class="col-md-6 survey">
        <div class="card">
          <h4 class="card-header">
            Kumpulan Survey
          </h4>
        <ul class="list-group list-group-flush">

          @foreach ($datas as $data)
          <a href="{{url('bankdata-click/'.$data->id)}}">
            <li class="list-group-item">
              <div class="row">
                <div class="col-md-11" style="width: 100%; overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{{ $data->judul }}</div>
                <div class="col-md-1"><i class="fa fa-chevron-right"></i></div>
              </div>
            </li>
          </a>
          @endforeach

        </ul>
        <div class="card-footer text-right">
          <a href="{{url('bankdata-index')}}">Lihat Selengkapnya <i class="fa fa-angle-double-right"></i></a>
        </div>
      </div>
      </div>

      <div class="col-md-6 database">
        <div class="card">
          <h4 class="card-header">Data Base</h4>
        </div>

          <ul class="list-group list-group-flush">
              <a href="{{url('halaman-staff')}}"><li class="list-group-item"><div class="logo"><i class="fa fa-file-text fa-2x"></i></div>
              <h5>Data Staff BEM KM</h5></li></a>
              <a href="{{url('halaman-fakultas')}}"><li class="list-group-item"><div class="logo"><i class="fa fa-file-text fa-2x"></i></div>
              <h5>Data Fakultas</h5></li></a>
              <a href="{{url('halaman-ukm')}}"><li class="list-group-item"><div class="logo"><i class="fa fa-file-text fa-2x"></i></div>
              <h5>Data UKM</h5></li></a>
          </ul>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>

<section id="foot-bd">
  <div class="container-fluid">
    <div class="row">

      <div class="col-md-5 ongoing">
        <div class="card">
          <h4 class="card-header">
            On Going Survey
          </h4>
        <ul class="list-group list-group-flush">

         @foreach ($going as $data)
           
          <a href="{{url('bacaon/'.$data->id)}}"><li class="list-group-item" style="width: 100%; overflow: hidden; white-space: nowrap; text-overflow: ellipsis;"><i class="fa fa-calendar">&emsp;{{ $data->judul }}</i></li></a>
         @endforeach

        </ul>
      </div>
    </div>
      <div class="col-md-4">
        <div class="kebijakan">
        <h5>Kebijakan & Privacy</h5>
        <hr>
        <ul>
            @foreach ($kebijakan as $key => $data)

            <li>{{$data->nama}}</li>
          @endforeach

        </ul>
      </div>
      </div>

      <div class="col-md-3">
        <div class="kontak">
        <h5>Kontak Kami</h5>
        <hr>

    <div class="row">
      <label for="example-text-input" class="col-1 col-form-label"><i class="fa fa-instagram"></i></label>
      <div class="col-10">
        <p>{{ $kontak->ig or ''}}</p>
      </div>
    </div>

    <div class="row">
      <label for="example-text-input" class="col-1 col-form-label"><i class="fa fa-phone"></i></label>
      <div class="col-10">
        <p>{{ $kontak->no_hp or ''}}</p>
      </div>
    </div>

    <div class="row">
      <label for="example-text-input" class="col-1 col-form-label"><i class="fa fa-envelope"></i></label>
      <div class="col-10">
        <div style="overflow: auto;">{{$kontak->email or ''}}</div>
      </div>
    </div>
      </div>
      </div>

    </div>
  </div>
</section>

   <script src="{{ asset('assets/jquery/dist/jquery.min.js')}}"></script>
   <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
   <script src="{{ asset('assets/bootstrap/js/bootstrap.bundle.js') }}"></script>
   <script src="{{ asset('assets/js/index.js') }}"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>

</body>
</html>
