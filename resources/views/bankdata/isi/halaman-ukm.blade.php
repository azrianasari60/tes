@include('bankdata/isi/headerdata')

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="card table-data">
				<h4 class="card-header">Data UKM</h4>
				<div class="card-body">
					<table>
						<tr>
							<th>Nama UKM</th>
							<th>Deskripsi</th>
							<th>Pengurus</th>
							<th>Nomor HP</th>
						</tr>
						@foreach ($ukm as $data)

						<tr>
							<td>{{$data->nama_ukm}}</td>
							<td>{{ $data->deskripsi }}</td>
							<td>{{$data->nama}}</td>
							<td>{{$data->no_hp}}</td>
						</tr>
					@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@include('bankdata/isi/footer')
