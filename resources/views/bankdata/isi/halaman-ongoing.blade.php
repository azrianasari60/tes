<section id="baca-ongoing">
@include('bankdata/isi/headerdata')
<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59fa9249f809021c"></script>



    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="baca" class="card">
                    <div class="card-header"><h4 class="text-justify">{{$dataongoing->judul}}</h4>
                    <small><i class="fa fa-calendar"></i>&nbsp;{{ $dataongoing->created_at }}</small>
                </div>
                    <div class="card-body">
                    <div class="addthis_inline_share_toolbox_k3xw"></div>
                        <img src="{{ asset('images/going/'.$dataongoing->foto) }}" style="width: 100%; margin-bottom: 20px;">
                        <h4>Deskripsi :</h4>
                        <p>{!! $dataongoing->deskripsi !!}</p>
                    </div>
                </div>

            </div>
            <div class="col-md-4">

            </div>
        </div>
    </div>

</section>

@include('bankdata/isi/footer')