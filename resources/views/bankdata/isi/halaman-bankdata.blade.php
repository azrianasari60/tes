@include('bankdata/isi/header')

<section id="halaman-berita">
  <div class="container">
    @if(count($survey) == 0)
       <p>
       Data Survey KOSONG
       </p>
@else
    @foreach ($survey as $data)
        {{-- expr --}}
    <div class="card">
      <div class="row">
         <div class="col-md-3">
          <div class="card-img-bottom">
            <img src="{{ asset('images/datasurvey/'.$data->foto) }}" style="width: 100%; height: 100%;">
          </div>
        </div>
        <div class="col-md-9">
          <div class="card-block">
            <h4 class="card-title">{{ $data->judul }}</h4>
            <p class="card-text">{{ $data->deskripsi }}</p>
            <a href="{{url('bankdata-click/'.$data->id)}}" class="btn btn-primer">Read More <i class="fa fa-angle-double-right"></i></a>
          </div>
        </div>
      </div>
    </div>
    <hr>
    @endforeach
@endif

{!! $survey->render() !!}


  </div>
</section>

@include('bankdata/isi/footer')
