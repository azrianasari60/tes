@include('bankdata/isi/headerdata')

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="card table-data">
				<h4 class="card-header">Data Staff BEM KM</h4>
				<div class="card-body">
					<table>
						<tr>
							<th>Nama</th>
							<th>Angkatan</th>
							<th>Fakultas</th>
						</tr>
						@foreach ($staf_bem as $data)

						<tr>
							<td>{{ $data->nama }}</td>
							<td>{{ $data->angkatan }}</td>
							<td>{{$data->fakultas->fakultas}}</td>
						</tr>
					@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@include('bankdata/isi/footer')
