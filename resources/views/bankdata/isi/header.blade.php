<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="robots" content="all,follow">
    <title>BEM KM UNMUL | BANK DATA</title>
    <link rel="icon" type="image/png" href="assets/img/logo_bem.png">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/bootstrap/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/font-awesome/css/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">

    <!-- <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css"> -->
</head>

<header id="bankdata-head">

        <div class="berita-banner" style="background-image: url({{asset('images/banner.jpg')}});"></div>

        <div class="container-fluid header">
          <div class="row">

            <div class="col-md-6">
              <a href="{{url('bankdata')}}">
                <h5 style="color: white; margin: 5px 0;"><i class="fa fa-angle-double-left"> Halaman Utama</i></h5>
              </a>
            </div>

            <div class="col-md-5 offset-md-1">
              <form class="input-group" action="{{ url('data/searching') }}" method="get">
                <input type="text" class="form-control mr-2" name="q" placeholder="Cari Data ...">
                <span><select class="form-control mr-2" name="kategori" id="exampleSelect1" style="width: 95%;">
                        <option value="semua" >Semua Kategori</option>
                        @foreach ($kategori as $data)
                       <option value="{{ $data->id }}">{{ $data->nama }}</option>

                       @endforeach 
                       </select></span>
                <span class="input-group-btn">
                  <button class="btn btn-secondary mr-2" type="submit"><i class="fa fa-search"></i></button>
                </span>
              </form>
          </div>

            </div>

          </div>

</header>

<body>
