<section id="baca">
@include('bankdata/isi/header')
<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59fa9249f809021c"></script>



    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="baca" class="card">
                    <div class="card-header"><h4 class="text-justify">{{$datasurvey->judul}}</h4>
                    <small><i class="fa fa-calendar"></i>&nbsp;{{ $datasurvey->created_at }}</small>
                </div>
                    <div class="card-body">
                    <div class="addthis_inline_share_toolbox_k3xw"></div>
                        <img src="{{ asset('images/datasurvey/'.$datasurvey->foto) }}" style="width: 100%; margin-bottom: 20px;">
                        <h4>Deskripsi :</h4>
                        <p>{{ $datasurvey->deskripsi }}</p>
                    </div>
                    <!-- <div class="card-footer">
                        <span class="bagikan">
                            <div class="media">
                              <ul>
                                <li class="bagi">Bagikan di :</li>
                                <li><a href=""><div class="fb"><i class="fa fa-facebook"></i></div></a></li>
                                <li><a href=""><div class="tw"><i class="fa fa-twitter"></i></div></a></li>
                                <li><a href=""><div class="ig"><i class="fa fa-instagram"></i></div></a></li>
                              </ul>
                            </div>
                        </span>
                    </div> -->
                </div>

            </div>
            <div class="col-md-4">

            </div>
        </div>
    </div>

</section>

@include('bankdata/isi/footer')