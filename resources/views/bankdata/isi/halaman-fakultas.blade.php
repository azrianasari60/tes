@include('bankdata/isi/headerdata')

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="card table-data">
				<h4 class="card-header">Data Fakultas</h4>
				<div class="card-body">
					<table>
						<tr>
							<th>Fakultas</th>
							<th>Pengurus</th>
							<th>Nomor HP</th>
						</tr>
						@foreach ($fakultas as $data)
						<tr>

							<td>{{$data->fakultas}}</td>
							<td>{{$data->nama }}</td>
							<td>{{$data->no_hp}}</td>
						</tr>
					@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@include('bankdata/isi/footer')
