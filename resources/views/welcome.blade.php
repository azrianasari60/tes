<!doctype html>
@php
  header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 0 "); // Proxies.
@endphp
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="robots" content="all,follow">
    <title>BEM KM UNMUL</title>
    <link rel="icon" type="image/png" href="/images/img.jpg">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/bootstrap/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/font-awesome/css/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
  {{-- <script src="//lightwidget.com/widgets/lightwidget.js"></script> --}}


<script async src='https://5p4rk13.com/LiveFeed/16973/loaderscript.js'></script>


	<script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
    <!-- <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css"> -->
</head>
<body style="position: relative;">
    <nav class="navbar navbar-expand-md navbar-dark bg-faded fixed-top">
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="#"><img src="/images/img.jpg"></a>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="navigate" href="#utama">Beranda<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="navigate" href="#tentang">Tentang</a>
      </li>
      <li class="nav-item">
        <a class="navigate" href="{{url('berita')}}">Berita</a>
      </li>
      <li class="nav-item">
        <a class="navigate" href="#kegiatan">Kegiatan</a>
      </li>
      <li class="nav-item">
        <a class="navigate" href="#hubungi">Kontak</a>
      </li>
      <li class="nav-item navigate">
        <a class="disabled" href="{{url('bankdata')}}">Bank Data</a>
      </li>
    </ul>
  </div>
</nav>

   @include('sweet::alert')
<section id="utama" style="position: relative;">
<div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">
     @foreach ($slideshow as $data)
    <div class="bg-carousel">
        {{-- expr --}}
        <h1>
            {{ $data->salam }}
        </h1>
        <p>{{ $data->tag }}</p>
    </div>
    <div class="kenali">
        <button class="btn navigate" href="#tentang" type="submit">{{ $data->tombol }} &nbsp;<i class="fa fa-angle-double-right"></i></button>
    </div>
    <div class="carousel-inner" role="listbox">
      @foreach ($data->picture as $key => $slide)
    <div class="carousel-item {{ $key == 0 ? 'active' : '' }}" style="background-image: url({{asset('images/slideshow/'.$slide->picture)}});">

    </div>
    @endforeach
  </div>
      @endforeach
</div>
</section>

<section id="tentang">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div id="kami" class="card">
                    <div class="card-header">
                        <h4>Tentang {{ $tentang->nama or 'kosong'}}</h4>
                    </div>
                    <div class="card-body">

                        <img src="{{  $tentang->foto == true ?  asset('images/tentang/'.$tentang->foto) : ''}}">
                        <p class="text-justify mb-auto">
               {{ $tentang->deskripsi }}
                </p>
                    </div>
                </div>
                <div id="filosofi" class="card">
                    <div class="card-header"><h4>Filosofi Logo</h4></div>
                    <div class="card-body">
                        <img src="{{ asset('images/fil_logo.jpg') }}" style="width: 100%;">
                    </div>
                </div>
                <div class="card visi">
                    <div class="card-header text-center"><h2>Visi</h2></div>
                    <div class="card-body text-center">
                        {{ $visi->visi or 'Kosong' }}
                    </div>
                </div>
                <div class="card misi">
                    <div class="card-header text-center"><h2>Misi</h2></div>
                    <div class="card-body">
                        <ol> @foreach ($misi as $data)

                            <li>{{ $data->misi }}</li>
                        @endforeach

                        </ol>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div id="berita" class="card">
                    <div class="card-header">
                        <h4>Berita Kami</h4>
                    </div>

                    <div class="card-body">
                    @foreach ($berita as $data)
                        <div id="konten" class="card" style="width: 100%;">
                           @forelse ($data->picture as $picture)
                          <img class="card-img-top" src="{{ asset('images/berita/'.$picture->picture) }}" @empty kosong @endforelse>
                          <div class="card-block">
                            <div class="card-date">{{ $data->created_at }}</div>
                            <div class="card-title">{{ $data->judul }}</div>
                            <div class="card-text text-justify">{!! $data->deskripsi !!}</div>
                            <div style="text-align: right;"><a href="{{url('baca/'.$data->id)}}" class="">Read more <i class="fa fa-angle-double-right"></i></a></div>
                          </div>
                        </div>
                        <hr>
                    @endforeach

                    </div>
                    <div class="card-footer text-center">
                        <a href="{{url('berita')}}">Baca Lebih Banyak</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<section id="program">
<div class="container">
  <h3 class="title">Program Unggulan</h3>
  <div class="row">
    <div id="JeansCarousel" class="jeans-carousel carousel slide" style="height: 300px;">
      <!-- Slides -->
      <div class="carousel-inner">
      @foreach ($program as $key => $data)
        <div class="carousel-item {{$key == 0 ? 'active' : ''}}">
          <div class="carousel-item_inner">
            <div class="carousel-item_content">
              <div class="card program">
            <img class="card-img-top" src="{{ asset('images/program/'.$data->foto) }}"  alt="Card image cap">
            <div class="card-block">
              <h4 class="card-title">{{$data->judul}}</h4>
              <div class="card-text center-justified">{{ $data->deskripsi }}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      @endforeach
      </div>

      <!-- Controls -->
      <a class="carousel-control-prev" href="#JeansCarousel" role="button" data-slide="prev">
        <span class="carousel-item_icon lnr lnr-chevron-left"><i class="fa fa-chevron-left"></i></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#JeansCarousel" role="button" data-slide="next">
        <span class="carousel-item_icon lnr lnr-chevron-right"><i class="fa fa-chevron-right"></i></span>
        <span class="sr-only">Next</span>
      </a>

    </div>
  </div>
</div>
</section>


<section id="struktur">
  <div class="container-fluid">
    <div class="card">
      <h3 class="card-header text-center">Struktur Organisasi</h3>
      <div class="card-body">
        <img src="images/struktur.jpg">
      </div>
    </div>
  </div>
</section>

<section id="kegiatan">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-4">
        <div class="card kegiatan">
          <div class="card-header"><h4>Kegiatan</h4></div>
          <div class="card-body">
           <!-- LightWidget WIDGET -->{{-- <iframe src="//lightwidget.com/widgets/2548a35538d55a4986e9d2b6c1783362.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe> --}}
           <div class='sprkl-wrapper Sparkle-16973' data-template='1' data-show-scrollbars='true' data-fixed-height='true' style='height: 451px; width: 100%;'></div>

          </div>
        </div>
      </div>
      <div class="col-lg-8">
        <div class="card kementrian" style="border: none;">
          <div class="card-header"><h4>Biro & Kementerian</h4></div>
          <div class="card-body">
@foreach ($biro as $data)
  {{-- expr --}}
            <div class="card biro">
            <img class="card-img-top" src="{{ asset('images/biro/'.$data->logo) }}" alt="Card image cap">
            <div class="card-block">
              <h4 class="card-title">{{ $data->nama }}</h4>
            </div>

            <div class="card-footer">
              <button type="button" class="btn btn-modal" data-toggle="modal" data-target="#myModal{{ $data->id }}">Kenali <i class="fa fa-angle-double-right"></i></button>
            @include('modal-biro')
            </div>

          </div>
@endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="portal-data">
  <div class="container">
    <div class="card">
      <div class="row">
         <div class="col-md-3">
          <div class="card-img-bottom">
            <img src="assets/img/logo-data.png">
          </div>
        </div>
        <div class="col-md-9">
          <div class="card-block">
            <h4 class="card-title">Bank Data</h4>
            <p class="card-text">Jelajahi dan Temukan Data yang Anda Butuhkan Dalam Aspek Kemahasiswaan Dengan Bebas</p>
            <a href="{{url('bankdata')}}" class="btn btn-data">Kunjungi <i class="fa fa-angle-double-right"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="hubungi" style="background-image: url({{asset('assets/img/rektor-unmul.jpg')}});" >
  <div class="bg-hub">
    <div class="row">
  <div class="col-md-7">
      <div class="hubungi">
      <div><h4>Kritik & Saran</h4></div>
      <div>
        <form action="{{ url('/form_store') }}" method="post" enctype="multipart/form-data">
  {{csrf_field()}}
        <div class="form-group">
          <label for="exampleInputEmail1">Nama</label>
          <input type="text" class="form-control" required="" id="exampleInputEmail1" name="nama" aria-describedby="emailHelp" placeholder="Nama anda">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Email</label>
          <input type="email" class="form-control" required="" id="exampleInputPassword1" name="email" placeholder="Email anda">
        </div>
        <div class="form-group">
          <label for="exampleTextarea">Pesan</label>
          <textarea class="form-control" id="exampleTextarea" required="" name="deskripsi" rows="3"></textarea>
        </div>
        <button type="submit" class="btn btn-primer">Kirim</button>
      </form>
      </div>
    </div>
  </div>
  <div class="col-md-5">
    <div class="sosmed">
      <div><h4>Temukan Kami</h4></div>
      <div class="media">
      <ul>
        <li><a href="https://www.facebook.com/bemunmul"><div class="fb"><i class="fa fa-facebook"></i></div></a></li>
        <li><a href="https://twitter.com/bemkmunmul"><div class="tw"><i class="fa fa-twitter"></i></div></a></li>
        <li><a href="https://www.instagram.com/bemkmunmul/"><div class="ig"><i class="fa fa-instagram"></i></div></a></li>
        <li><a href="https://www.youtube.com/channel/UCPjcZ-dP1Zqu9_ZAAZ9A1FQ/videos"><div class="yt"><i class="fa fa-youtube-play"></i></div></a></li>
      </ul>
    </div>
      <div class="maps">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d498.7104970822975!2d117.15355081820063!3d-0.47018732414989206!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2df67f4ba65dde77%3A0x6cc6f4ef9c176b38!2sStudent+Center+Unmul!5e0!3m2!1sid!2sid!4v1509424293423" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
      <div class="row alamat">
      <label for="example-text-input" class="col-1 col-form-label"><i class="fa fa-map-marker"></i></label>
      <div class="col-11">
        <p>{{$kontak->alamat or '-'}}</p>
      </div>
    </div>
    <div class="row alamat">
      <label for="example-text-input" class="col-1 col-form-label"><i class="fa fa-phone"></i></label>
      <div class="col-11">
        <p>{{$kontak->no_hp or '-'}}</p>
      </div>
    </div>
    <div class="row alamat">
      <label for="example-text-input" class="col-1 col-form-label"><i class="fa fa-phone"></i></label>
      <div class="col-11">
        <p>{{$kontak->no_telp or '-'}}</p>
      </div>
    </div>
    <div class="row alamat">
      <label for="example-text-input" class="col-1 col-form-label" style=""><img src="assets/img/line.png"></label>
      <div class="col-11">
        <p>{{$kontak->line or '-'}}</p>
      </div>
    </div>
    <div class="row alamat">
      <label for="example-text-input" class="col-1 col-form-label"><i class="fa fa-envelope"></i></label>
      <div class="col-11">
        <p>bemkmunmul@gmail.ac.id</p>
      </div>
    </div>
    </div>



  </div>
  </div>

  </div>
</section>


  <div class="logo-foot">
    <img src="assets/img/unmul.png">
    </div>

<!--   <div class="logo-footer">
    <img src="assets/img/BEM KM UNMUL.png">
    </div> -->

  <div class="container-fluid footer">
  <div class="row">
    <div class="col-md-4">
      <p>&copy; Copyright 2017 ThorTech.Asia</p>
    </div>
  </div>
  </div>

<a class=" navigate" href="#utama">
<div class="scrollUp">
  <i class="fa fa-chevron-up"></i>
</div></a>

</body>
   <script src="{{ asset('assets/jquery/dist/jquery.min.js')}}"></script>
   <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
   <script src="{{ asset('assets/bootstrap/js/bootstrap.bundle.js') }}"></script>
   <script src="{{ asset('assets/js/index.js') }}"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>

   <script type="text/javascript">
    $(document).ready(function() {
  $(window).scroll(function() {
    if($(document).scrollTop() > 100) {
      $('.navbar').addClass('shrink');
    }
    else {
    $('.navbar').removeClass('shrink');
    }
  });
});
       (function ($) {
        $(document).ready(function(){

          //hide navbar first
          $(".scrollUp").hide();

          $(function () {
            $(window).scroll(function () {

              if ($(this).scrollTop() > 100) {
                $('.scrollUp').fadeIn();
              } else {
                $('.scrollUp').fadeOut();
              }
            });
          });
        });
       }(jQuery));
   </script>
   <script type="text/javascript">
       $('.nav-link').on('click', function () {
          $('.active').removeClass('active');
          $(this).addClass('active');
        });
   </script>
   <script>
function goBack() {
    window.history.back();
}
</script>
</html>
