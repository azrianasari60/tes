@extends('layouts.default-404')

@section('content')

<div class="container" id="404"> 
  <div class="row"> 
    <div class="col s12 center">
      <h1 id="number">0</h1>
      <h2>Kamu Tersesat?</h2>
      <a href="/" class="large btn cyan darken-2">BEMKM</a>
    </div>
  </div>
  <div class="row">
  </div>      
</div>  
@stop

@section('js')
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="{{ asset('js/materialize_2.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script src="{{ asset('js/jquery.animatenumber.min.js') }}"></script>
<script src="{{ asset('js/jquery.appear.js') }}"></script>
@stop