

    <div class="error-page">
        <h2 class="headline text-yellow"> 404</h2>
        <div class="error-content">
            <h3><i class="fa fa-warning text-yellow"></i> Oops! Page Not Found.</h3>
                <br>
                <br>
        </div><!-- /.error-content -->
    </div><!-- /.error-page -->
