<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/','DController@index');

Route::get('baca/{id}','DController@showberita' );

Route::get('berita','DController@berita');

Route::get('berita/cari','DController@search');
Route::post('/form_store','DController@store');

Route::get('bankdata','BankdataController@index');
Route::get('bacaon/{id}','BankdataController@bacaon');
Route::get('bankdata-indexall','BankdataController@showall');

Route::get('bankdata-index/{id}', 'BankdataController@tampilsurvey');
Route::get('bankdata-index', 'BankdataController@kumpulansurvey');
Route::get('bankdata-click/{id}', 'BankdataController@baca');
Route::get('data/search', 'BankdataController@search');
Route::get('data/searching', 'BankdataController@searching');

Route::get('halaman-staff','DatabaseController@staf' );
Route::get('halaman-ukm','DatabaseController@ukm' );
Route::get('halaman-fakultas','DatabaseController@fakultas' );




Auth::routes();
Route::group(['middleware'=>'auth'],function (){
    /*
    |--------------------------------------------------------------------------
    | Dashboard
    |--------------------------------------------------------------------------
    */
      Route::get('admin/dashboard','HomeController@index')->name('dashboard');
      /**
       * SlideShow
       */
      Route::get('admin/slideshow','SlideshowController@index')->name('slideshow');
      Route::get('admin/slideshow/create','SlideshowController@create')->name('slideshow_create');
      Route::post('admin/slideshow/store','SlideshowController@store')->name('slideshow_store');
      Route::get('admin/slideshow/aktiv/{id}','SlideshowController@aktiv');
      Route::get('admin/slideshow/edit/{id}','SlideshowController@edit');
      Route::post('slideshow/edit/{id}','SlideshowController@update');
      Route::get('admin/slideshow/delete/{id}','SlideshowController@destroy');
      Route::get('deleteslideshow/{id}','SlideshowController@deleteOption');
      /**
       * User
       */
      Route::get('admin/user','UserController@index');
      Route::get('user/create','UserController@create');
      Route::post('user/store','UserController@store');
      Route::get('user/edit/{id}','UserController@edit');
      Route::post('user/edit/{id}','UserController@update');
      Route::get('user/delete/{id}','UserController@destroy');
      Route::get('deleteuser/{id}','UserController@deleteOption');
        /**
       * Berita
       */
      Route::get('admin/berita','BeritaController@index');
      Route::get('berita/create','BeritaController@create');
      Route::post('berita/store','BeritaController@store');
      Route::get('berita/aktiv/{id}','BeritaController@aktiv');
      Route::get('berita/edit/{id}','BeritaController@edit');
      Route::post('berita/edit/{id}','BeritaController@update');
  	  Route::get('berita/show/{id}','BeritaController@show');
  	  Route::get('berita/delete/{id}','BeritaController@destroy');
      Route::get('deleteberita/{id}','BeritaController@deleteOption');


    /**
     * Biro
     */
      Route::get('admin/biro','BiroController@index');
      Route::get('biro/create','BiroController@create');
      Route::post('biro/store','BiroController@store');
      Route::get('biro/edit/{id}','BiroController@edit');
      Route::post('biro/update/{id}','BiroController@update');
      Route::get('biro/hapus/{id}','BiroController@destroy');
      Route::get('deletebiro/{id}','BiroController@deleteOption');
      /**
     * Kontak
     */
       Route::get('admin/kontak/aktiv/{id}','KontakController@aktiv');
      Route::get('admin/kontak','KontakController@index');
      Route::get('kontak/create','KontakController@create');
      Route::post('kontak/store','KontakController@store');
      Route::get('kontak/edit/{id}','KontakController@edit');
      Route::post('kontak/update/{id}','KontakController@update');
      Route::get('kontak/hapus/{id}','KontakController@destroy');
      Route::get('deletekontak/{id}','KontakController@deleteOption');

      /**
       * Visi
       */
      Route::get('admin/visi','VisiController@index');
      Route::get('visi/edit/{id}','VisiController@edit');
      Route::post('visi/update/{id}','VisiController@update');
      Route::get('visi/create','VisiController@create');
      Route::post('visi/store','VisiController@store');
      Route::get('visi/aktiv/{id}','VisiController@aktiv');
      Route::get('visi/delete/{id}','VisiController@destroy');
      Route::get('deletevisi/{id}','VisiController@deleteOption');

      /**
       * Misi
       */
      Route::get('admin/misi','MisiController@index');
      Route::get('misi/create','MisiController@create');
      Route::post('misi/store','MisiController@store');
      Route::get('misi/edit/{id}','MisiController@edit');
      Route::post('misi/update/{id}','MisiController@update');
      Route::get('misi/hapus/{id}','MisiController@destroy');
      Route::get('deletemisi/{id}','MisiController@deleteOption');
        /**
       * gambar
       */
      Route::get('admin/logo', function () {
          return view('admin/logo');
      });
      Route::get('admin/banner', function () {
          return view('admin/banner');
      });
      Route::get('banner/edit','LogoController@buat');
      Route::post('banner/edit','LogoController@buatb');
      Route::get('admin/edit','LogoController@create');
      Route::post('logo/edit','LogoController@index');
      Route::get('admin/struktur', function () {
          return view('admin/struktur');
      });
      Route::get('struktur/edit','LogoController@createp');
      Route::post('struktur/edit','LogoController@store');
      Route::get('admin/filsuf', function () {
          return view('admin/fil_logo');
      });
      Route::get('fil_logo/edit','LogoController@fil');
      Route::post('fil_logo/edit','LogoController@filsuf');


       /**
       * Program
       */
      Route::get('admin/program','ProgramController@index');
      Route::get('program/create','ProgramController@create');
      Route::post('program/store','ProgramController@store');
      Route::get('program/edit/{id}','ProgramController@edit');
      Route::post('program/edit/{id}','ProgramController@update');
      Route::get('program/delete/{id}','ProgramController@destroy');
      Route::get('deleteprogram/{id}','ProgramController@deleteOption');
       /**
       * Tentang
       */
      Route::get('admin/tentang','TentangController@index');
      Route::get('tentang/create','TentangController@create');
      Route::post('tentang/store','TentangController@store');
      Route::get('tentang/edit/{id}','TentangController@edit');
      Route::post('tentang/edit/{id}','TentangController@update');
      Route::get('tentang/delete/{id}','TentangController@destroy');
      Route::get('deletetentang/{id}','TentangController@deleteOption');
      Route::get('tentang/aktiv/{id}','TentangController@aktiv');

      /**
       * Pengaduan
       */
      Route::get('admin/pengaduan','PengaduanController@index');
      Route::get('alert/pengaduan','PengaduanController@alert');
      Route::get('pengaduan/delete/{id}','PengaduanController@destroy');
      Route::get('pengaduan/show/{id}/{notification}','PengaduanController@show');
      Route::get('pengaduan/show/{id}','PengaduanController@shows');
      Route::get('deletepengaduan/{id}','PengaduanController@deleteOption');
      Route::get('showall', function()
      {
        return View('admin/notification/showallnotif');
      });


});
Route::group(['middleware'=>'auth'],function (){
      /**
       * Going
       */
      Route::get('admin/going','GoingController@index');
      Route::get('going/create','GoingController@create');
      Route::post('going/store','GoingController@store');
      Route::get('going/edit/{id}','GoingController@edit');
      Route::post('going/edit/{id}','GoingController@update');
      Route::get('going/delete/{id}','GoingController@destroy');
      Route::get('deletegoing/{id}','GoingController@deleteOption');

      /**
       * Kebijakan
       */
      Route::get('admin/kebijakan','KebijakanController@index');
      Route::get('kebijakan/create','KebijakanController@create');
      Route::post('kebijakan/store','KebijakanController@store');
      Route::get('kebijakan/edit/{id}','KebijakanController@edit');
      Route::post('kebijakan/edit/{id}','KebijakanController@update');
      Route::get('kebijakan/delete/{id}','KebijakanController@destroy');
      Route::get('deletekebijakan/{id}','KebijakanController@deleteOption');


      /**
       * Slidebank
       */
      Route::get('admin/slide','SlideController@index');
      Route::get('slide/create','SlideController@create');
      Route::post('slide/store','SlideController@store');
      Route::get('slide/edit/{id}','SlideController@edit');
      Route::post('slide/edit/{id}','SlideController@update');
      Route::get('slide/delete/{id}','SlideController@destroy');
      Route::get('deleteslide/{id}','SlideController@deleteOption');
      Route::get('slide/aktiv/{id}','SlideController@aktiv');
           /**
       * Fakultas
       */
      Route::get('admin/fakultas','FakultasController@index');
      Route::get('fakultas/create','FakultasController@create');
      Route::post('fakultas/store','FakultasController@store');
      Route::get('fakultas/edit/{id}','FakultasController@edit');
      Route::post('fakultas/edit/{id}','FakultasController@update');
      Route::get('fakultas/delete/{id}','FakultasController@destroy');
      Route::get('deletefakultas/{id}','FakultasController@deleteOption');
            /**
       * Staff BEMKM
       */
      Route::get('admin/staf','StafController@index');
      Route::get('staf_bem/create','StafController@create');
      Route::post('staf_bem/store','StafController@store');
      Route::get('staf_bem/edit/{id}','StafController@edit');
      Route::post('staf_bem/update/{id}','StafController@update');
      Route::get('staf_bem/delete/{id}','StafController@destroy');
      Route::get('deletestaf_bem/{id}','StafController@deleteOption');

      /**
       * Kategori
       */

      Route::get('admin/kategori','KategoriController@index');
      Route::get('kategori/create','KategoriController@create');
      Route::post('kategori/store','KategoriController@store');
      Route::get('kategori/edit/{id}','KategoriController@edit');
      Route::post('kategori/edit/{id}','KategoriController@update');
      Route::get('kategori/delete/{id}','KategoriController@destroy');
      Route::get('deletekategori/{id}','KategoriController@deleteOption');

           /**
       * Data Survey
       */

      Route::get('admin/datasurvey','DatasurveyController@index');
      Route::get('datasurvey/create','DatasurveyController@create');
      Route::post('datasurvey/store','DatasurveyController@store');
      Route::get('datasurvey/edit/{id}','DatasurveyController@edit');
      Route::post('datasurvey/edit/{id}','DatasurveyController@update');
      Route::get('datasurvey/delete/{id}','DatasurveyController@destroy');
      Route::get('deletedatasurvey/{id}','DatasurveyController@deleteOption');
      Route::get('admin/searching', 'DatasurveyController@searching');

      /**
       * Survey
       */
      Route::get('admin/survey','SurveyController@index');
      Route::get('survey/create','SurveyController@create');
      Route::post('survey/store','SurveyController@store');
      Route::get('survey/edit/{id}','SurveyController@edit');
      Route::post('survey/update/{id}','SurveyController@update');
      Route::get('survey/delete/{id}','SurveyController@destroy');
      Route::get('deletesurvey/{id}','SurveyController@deleteOption');
      /**
       * ukm
       */
      Route::get('admin/ukm','UkmController@index');
      Route::get('ukm/create','UkmController@create');
      Route::post('ukm/store','UkmController@store');
      Route::get('ukm/edit/{id}','UkmController@edit');
      Route::post('ukm/update/{id}','UkmController@update');
      Route::get('ukm/delete/{id}','UkmController@destroy');
      Route::get('deleteukm/{id}','UkmController@deleteOption');

      /**
     * Kontakdata
     */
      Route::get('admin/kontakdata/aktiv/{id}','KontakdataController@aktiv');
      Route::get('admin/kontakdata','KontakdataController@index');
      Route::get('kontakdata/create','KontakdataController@create');
      Route::post('kontakdata/store','KontakdataController@store');
      Route::get('kontakdata/edit/{id}','KontakdataController@edit');
      Route::post('kontakdata/update/{id}','KontakdataController@update');
      Route::get('kontakdata/hapus/{id}','KontakdataController@destroy');
      Route::get('deletekontakdata/{id}','KontakdataController@deleteOption');

});
Route::get('/markAsRead/{id}',function(){
    auth()->user()->unreadNotifications->markAsRead();
});
