<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
      'name' => 'Babul',
      'email' => 'arziqb@gmail.com',
      'password' => bcrypt('coba1234'),
      'status' => 'Admin',
  ]);

    }
}
