<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePicturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pictures', function (Blueprint $table) {
            $table->increments('id');
            $table->string('picture');
            $table->integer('slideshow_id')->unsigned()->nullable();
            $table->foreign('slideshow_id')->references('id')->on('slideshows')->onDelete('cascade');
             $table->integer('berita_id')->unsigned()->nullable();
            $table->foreign('berita_id')->references('id')->on('beritas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pictures');
    }
}
