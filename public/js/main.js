function markNotificationAsRead(notificationCount) {
    if(notificationCount !=='0'){
        $.get('/markAsRead/{$id}');
    }
}
jQuery(document).ready(function($){
	// browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 300,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 700,
		//grab the "back to top" link
		$back_to_top = $('.cd-top');

	//hide or show the "back to top" link
	$(window).scroll(function(){
		( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if( $(this).scrollTop() > offset_opacity ) { 
			$back_to_top.addClass('cd-fade-out');
		}
	});

	//smooth scroll to top
	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0 ,
		}, scroll_top_duration
		);
	});

	// shrink navbar

	$(window).scroll(function() {
		if ($(document).scrollTop() > 35) {
			$('nav').removeClass('navbar');
			$('nav').removeClass('z-depth-0');
			$('nav').addClass('shrink');
		} else {
			$('nav').removeClass('shrink');
			$('nav').addClass('navbar');
			$('nav').addClass('z-depth-0');
			
		}
	});

	$('.slider').slider({
		indicators: false
	});

	$('select').material_select();

	$('ul.tabs').tabs({
		swipeable: true 
	});

	$('.button-collapse').sideNav({
		menuWidth: 300,
		edge: 'left',
		closeOnClick: true,
		draggable: true
	});

	$('.materialboxed').materialbox();

	$("#facts").appear(function() {
		$("#number_1").animateNumber({
            number: 4464 // Change to your number			
        }, 2200);
		$("#number_2").animateNumber({
            number: 632 // Change to your number	
        }, 2200);
		$("#number_3").animateNumber({
            number: 1488 // Change to your number
        }, 2200);
		$("#number_4").animateNumber({
            number: 46 // Change to your number
        }, 2200);
		$("#number_5").animateNumber({
            number: 27 // Change to your number
        }, 2200);
	}, {
		accX: 0,
		accY: -150
	});

	$("#404").appear(function () {
		$("#number").animateNumber({
			number: 404
		}, 1200);
	}, {
		accX: 0,
		accY: -150
	});

	$(window).load(function() {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");;
	});
	
	$('.materialboxed').materialbox();      
});